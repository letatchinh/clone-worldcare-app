import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Toast from 'react-native-toast-message';
import { Provider } from 'react-redux';
import NavigatorStack from './src/core/NavigatorStack';
// import ReactNativeElementProvider from './src/core/ReactNativeElementProvider';
import { store } from './src/redux/store';
import { LogBox } from 'react-native';
import toastConfig from './src/core/ToastConfig';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import 'moment/locale/vi';// Import the Vietnamese locale

LogBox.ignoreAllLogs();//Ignore all log notifications


function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <GestureHandlerRootView style={{ flex: 1 }}> 
          <NavigatorStack />
          <Toast config={toastConfig} />
        </GestureHandlerRootView>
      </Provider>
    </SafeAreaProvider>
  );
}

export default App;
