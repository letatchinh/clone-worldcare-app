import {all} from 'redux-saga/effects';
import auth from './auth/task';
import geo from './geo/task';

export default function* rootSaga() {
  yield all([
    auth(),
    geo(),
  ]);
}
