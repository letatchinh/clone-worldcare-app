import { put, call, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import {geoActions} from './reducer'
function* getAreas() {
  try {
    const data = yield call(api.geo.getAreas);
    yield put(geoActions.getAreasSuccess(data));
  } catch (error) {
    yield put(geoActions.getAreasFailed(error));
  }
}

function* getCities() {
  try {
    const data = yield call(api.geo.getCities);
    yield put(geoActions.getCitiesSuccess(data));
  } catch (error) {
    yield put(geoActions.getCitiesFailed(error));
  }
}

function* getDistricts({ payload }) {
  try {
    const data = yield call(api.geo.getDistricts, payload);
    yield put(geoActions.getDistrictsSuccess(data));
  } catch (error) {
    yield put(geoActions.getDistrictsFailed(error));
  }
}

function* getWards({ payload }) {
  try {
    const data = yield call(api.geo.getWards, payload);
    yield put(geoActions.getWardsSuccess(data));
  } catch (error) {
    yield put(geoActions.getWardsFailed(error));
  }
}

function* addAddress({ payload }) {
  try {
    const data = yield call(api.geo.addAddress, payload);
    yield put(geoActions.addAddressSuccess(data));
  } catch (error) {
    yield put(geoActions.addAddressFailed(error));
  }
}


function* updateAddress({ payload }) {
  try {
    const data = yield call(api.geo.updateAddress, payload);
    yield put(geoActions.updateAddressSuccess(data));
  } catch (error) {
    yield put(geoActions.updateAddressFailed(error));
  }
}

function* deleteAddress({ payload : id }) {
  try {
    yield call(api.geo.deleteAddress, id);
    yield put(geoActions.deleteAddressSuccess(id));
  } catch (error) {
    yield put(geoActions.deleteAddressFailed(error));
  }
}



function* getAddress() {
  try {
    const data = yield call(api.cart.getMyAddresses);
    yield put(geoActions.getAddressSuccess(data));
  } catch (error) {
    yield put(geoActions.getAddressFailed(error));
  }
}

export default function* geoSaga() {
  yield takeLatest(geoActions.getAreasRequest, getAreas);
  yield takeLatest(geoActions.getCitiesRequest, getCities);
  yield takeLatest(geoActions.getDistrictsRequest, getDistricts);
  yield takeLatest(geoActions.getWardsRequest, getWards);
  yield takeLatest(geoActions.addAddressRequest, addAddress);
  yield takeLatest(geoActions.updateAddressRequest, updateAddress);
  yield takeLatest(geoActions.deleteAddressRequest, deleteAddress);
  yield takeLatest(geoActions.getAddressRequest, getAddress);
}
