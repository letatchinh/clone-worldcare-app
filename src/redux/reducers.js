import { persistReducer } from 'redux-persist';

import { combineReducers } from "redux";

import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from './auth/reducer';
import geo from './geo/reducer';
import layout from './layout/reducer';

const authPersistConfig = {
    key: 'auth',
    storage: AsyncStorage,
    blacklist: [
        'loginFailed',
        'isLoading',
        'isGetProfileLoading',
        'getProfileFailed',
        'updateProfileSuccess',
        'updateProfileFailed',
        'isUpdateProfileLoading',
      ]
  };
const layoutPersistConfig = {
    key: 'layout',
    storage: AsyncStorage,
    blacklist: [
      ]
  };
const rootReducer = combineReducers({
    auth: persistReducer(authPersistConfig, auth),
    layout: persistReducer(layoutPersistConfig, layout),
    geo,
});
export default rootReducer