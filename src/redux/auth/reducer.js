import { createSlice } from '@reduxjs/toolkit'
import { clearAsyncStorage } from '../../hook/utils';
import { formatPhone } from '../../utils/helper';
import { get } from 'lodash';
import { removeAxiosToken,setAxiosToken } from '../../core/axios'; 
const initialState = {
    isLoading: false,

    token: null,
    loginFailed: null,

    profile: null,
    isGetProfileLoading: false,
    getProfileFailed: null,

    updateProfileFailed: null,
    updateProfileSuccess: null,
    isUpdateProfileLoading: false,
}

export const auth = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        // LOGIN
        loginRequest: (state, { payload }) => {
            state.isLoading = true;
            state.token = null;
            state.loginFailed = null;
        },
        loginSuccess: (state, { payload }) => {
            state.token = payload.token;
            setAxiosToken(payload.token)
        },
        loginFailed: (state, { payload }) => {
            state.loginFailed = payload;
            state.isLoading = false;
        },
        logoutRequest: () => {
            // removeAxiosToken();
            removeAxiosToken();
            clearAsyncStorage(); // FIX ME: use await request if failed
            return initialState
        },

        // GET PROFILE

        getProfileRequest: (state, { payload }) => {
            state.isGetProfileLoading = true;
            state.getProfileFailed = null;
        },
        getProfileSuccess: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.profile = { ...payload, phoneNumberFormat: formatPhone(get(payload, 'phoneNumber', '')) };
        },
        getProfileFailed: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.getProfileFailed = payload;
        },


        // UPDATE PROFILE

        updateProfileRequest: (state, { payload }) => {
            state.isUpdateProfileLoading = true;
            state.updateProfileFailed = null;
        },
        updateProfileSuccess: (state, { payload }) => {
            state.isUpdateProfileLoading = false;
            state.updateProfileSuccess = payload;
        },
        updateProfileFailed: (state, { payload }) => {
            state.isUpdateProfileLoading = false;
            state.updateProfileFailed = payload;
        },

        // Refresh Link Ref

        refreshLinkRefRequest: (state) => {
            state.isUpdateProfileLoading = true;
        },
        refreshLinkRefSuccess: (state, { payload }) => {
            state.isUpdateProfileLoading = false;
            const newReferralLink = get(payload, 'data.referralLink');
            state.profile = { ...state.profile, referralLink: newReferralLink };
        },
        refreshLinkRefFailed: (state) => {
            state.isUpdateProfileLoading = false;
        },
    },
})

// Action creators are generated for each case reducer function
export const actions = auth.actions

export default auth.reducer