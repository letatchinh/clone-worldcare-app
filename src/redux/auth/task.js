import { put, call, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { actions } from './reducer';

function* login({ payload: user }) {
  try {
    const {token,userId} = yield call(api.auth.postSignIn, user);
    yield put(actions.loginSuccess({token,userId}));
  } catch (error) {
    yield put(actions.loginFailed(error));
  }
}


function* getProfile() {
  try {
    const profile = yield call(api.auth.getProfile);
    yield put(actions.getProfileSuccess(profile));
  } catch (error) {
    yield put(actions.getProfileFailed(error));
  }
}

function* updateProfile({payload}) {
  try {
    const profile = yield call(api.auth.postProfile,payload);
    yield put(actions.updateProfileSuccess(profile));
  } catch (error) {
    yield put(actions.updateProfileFailed(error));
  }
}
function* refreshLinkRef({payload : id}) {
  try {
    const profile = yield call(api.customerAccount.refreshLink,id);
    yield put(actions.refreshLinkRefSuccess(profile));
  } catch (error) {
    yield put(actions.refreshLinkRefFailed(error));
  }
}


export default function* userSaga() {
  yield takeLatest(actions.loginRequest, login);
  yield takeLatest(actions.getProfileRequest, getProfile);
  yield takeLatest(actions.updateProfileRequest, updateProfile);
  yield takeLatest(actions.refreshLinkRefRequest, refreshLinkRef);
}
