import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    scrollEnabled : true,
}

export const layoutSlice = createSlice({
    name: 'layout',
    initialState,
    reducers: {
    
        disabledScroll: (state) => {
            state.scrollEnabled = false;
        },
        enabledScroll: (state) => {
            state.scrollEnabled = true;
        },

        reset: () => initialState,
    },
})

// Action creators are generated for each case reducer function
export const layoutReducerAction = layoutSlice.actions

export default layoutSlice.reducer