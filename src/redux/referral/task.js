import { put, call, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { getExistProp } from '../../utils/helper';
import { referralReducerAction } from './reducer';

function* getRefs({ payload: query }) {
  try {
    const data = yield call(api.auth.getRefs, getExistProp(query));
    yield put(referralReducerAction.getRefsSuccess(data));
  } catch (error) {
    yield put(referralReducerAction.getRefsFailed(error));
  }
}
function* getInfinityScrollRefs({ payload: query }) {
  try {
    const data = yield call(api.auth.getRefs, getExistProp(query));
    yield put(referralReducerAction.getInfinityScrollRefsSuccess(data));
  } catch (error) {
    yield put(referralReducerAction.getInfinityScrollRefsFailed(error));
  }
}


function* getInfinityScrollRewards({ payload: query }) {
  try {
    const data = yield call(api.customerAccount.getRewards, getExistProp(query));
    yield put(referralReducerAction.getInfinityScrollRewardsSuccess(data));
  } catch (error) {
    yield put(referralReducerAction.getInfinityScrollRewardsFailed(error));
  }
}





export default function* referralSaga() {
  yield takeLatest(referralReducerAction.getRefsRequest, getRefs);
  yield takeLatest(referralReducerAction.getInfinityScrollRefsRequest, getInfinityScrollRefs);
  yield takeLatest(referralReducerAction.getInfinityScrollRewardsRequest, getInfinityScrollRewards);
}
