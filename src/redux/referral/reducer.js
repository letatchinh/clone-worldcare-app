import { createSlice } from '@reduxjs/toolkit';
import { get } from 'lodash';
import { getPaging } from '../../utils/helper';
const initialState = {
    isLoadingRef: false,
    listRef: [],
    getListRefFailed: null,
    pagingRef: null,

    isLoadingReward: false,
    listReward: [],
    getListRewardFailed: null,
    pagingReward: null,



}

export const referralSlice = createSlice({
    name: 'referral',
    initialState,
    reducers: {
        // GET LIST REF
        getRefsRequest: (state, { payload }) => {
            state.isLoadingRef = true;
            state.getListRefFailed = null;
        },
        getRefsSuccess: (state, { payload }) => {
            state.isLoadingRef = false;
            state.listRef = get(payload, 'docs', []);
            state.pagingRef = getPaging(payload);
        },
        getRefsFailed: (state, { payload }) => {
            state.isLoadingRef = false;
            state.getListRefFailed = payload;
        },

        // GET LIST REFS INFINITY SCROLL
        getInfinityScrollRefsRequest: (state, { payload }) => {
            state.isLoadingRef = true;
            state.getListRefFailed = null;
        },
        getInfinityScrollRefsSuccess: (state, { payload }) => {
            state.isLoadingRef = false;
            state.listRef = [...state.listRef, ...get(payload, 'docs', [])]
            state.pagingRef = getPaging(payload);
        },
        getInfinityScrollRefsFailed: (state, { payload }) => {
            state.isLoadingRef = false;
            state.getListRefFailed = payload;
        },

        // GET LIST REWARD INFINITY SCROLL
        getInfinityScrollRewardsRequest: (state) => {
            state.isLoadingReward = true;
            state.getListRewardFailed = null;
        },
        getInfinityScrollRewardsSuccess: (state, { payload }) => {
            state.isLoadingReward = false;
            state.listReward = [...state.listReward, ...get(payload, 'docs', [])];
            state.pagingReward = getPaging(payload);
        },
        getInfinityScrollRewardsFailed: (state, { payload }) => {
            state.isLoadingReward = false;
            state.getListRewardFailed = payload;
        },

        resetReward: (state) => {
            state.isLoadingReward = false;
            state.listReward = [];
            state.getListRewardFailed = null;
            state.pagingReward = null;
        },

        resetRefs: (state) => {
            state.isLoadingRef = false;
            state.listRef = [];
            state.getListRefFailed = null;
            state.pagingRef = null;
        },

        reset: () => initialState,
    },
})

// Action creators are generated for each case reducer function
export const referralReducerAction = referralSlice.actions

export default referralSlice.reducer