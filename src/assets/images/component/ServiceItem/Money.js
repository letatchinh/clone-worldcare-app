import React from 'react';

const Money = ({className}) => {
  return (
    <svg
      className={className}
      width="35"
      height="41"
      viewBox="0 0 35 41"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <mask
        id="mask0_55_3596"
        style="mask-type:luminance"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="33"
        height="24">
        <path d="M32.3705 0H0V23.6485H32.3705V0Z" fill="white" />
      </mask>
      <g mask="url(#mask0_55_3596)">
        <g filter="url(#filter0_d_55_3596)">
          <path
            d="M13.4358 7.28215L1.98849 16.4532L22.4604 40.6236L33.9077 31.4525L13.4358 7.28215Z"
            fill="#CBF0DD"
            stroke="#1D9DA0"
            stroke-miterlimit="10"
          />
          <path
            d="M10.0787 9.97116C11.6059 11.7747 14.3459 12.0316 16.2003 10.5463L13.4359 7.28259L10.0787 9.97116Z"
            fill="#1D9DA0"
          />
          <path
            d="M5.35578 14C6.88295 15.8022 6.61742 18.4684 4.76443 19.9523L2.00003 16.6886L5.35722 14H5.35578Z"
            fill="#1D9DA0"
          />
          <path
            d="M21.6689 28.3472C24.1649 26.3483 24.5208 22.7593 22.4654 20.3332C20.4101 17.907 16.7199 17.5595 14.2239 19.5584C11.7279 21.5574 11.3719 25.145 13.4273 27.5725C15.4827 30 19.1729 30.3462 21.6689 28.3472Z"
            fill="#1D9DA0"
          />
        </g>
        <g filter="url(#filter1_d_55_3596)">
          <path
            d="M27.0583 5.98268L12.6559 9.42578L20.3418 39.8356L34.7441 36.3925L27.0583 5.98268Z"
            fill="#CBF0DD"
            stroke="#1D9DA0"
            stroke-miterlimit="10"
          />
          <path
            d="M22.7273 6.74043C23.3 9.00883 25.6568 10.395 27.9877 9.83801L26.95 5.73117L22.7273 6.74043Z"
            fill="#1D9DA0"
          />
          <path
            d="M16.95 8.37774C17.5241 10.6461 16.0974 12.9369 13.765 13.4938L12.7273 9.387L16.95 8.37634V8.37774Z"
            fill="#1D9DA0"
          />
          <path
            d="M24.9813 28.4169C28.1218 27.6658 30.0408 24.5822 29.2686 21.5279C28.4964 18.475 25.3258 16.6086 22.1853 17.3597C19.0449 18.1107 17.1259 21.1943 17.8981 24.2472C18.6703 27.3015 21.8409 29.1679 24.9813 28.4169Z"
            fill="#1D9DA0"
          />
        </g>
      </g>
      <g filter="url(#filter2_d_55_3596)">
        <path
          d="M4.11071 19.6199H32.1753C32.8111 19.6199 33.3279 20.1224 33.3279 20.7408V39.3179C33.3279 39.9363 32.8111 40.4388 32.1753 40.4388H4.11071C3.47487 40.4388 2.95816 39.9363 2.95816 39.3179V20.7408C2.95816 20.1224 3.47487 19.6199 4.11071 19.6199Z"
          fill="#35D2B9"
        />
      </g>
      <path
        d="M30.9094 37.8801V38.3686H30.407"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M29.4253 38.3687H6.3699"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M5.87904 38.3686H5.37668V37.8801"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M5.37668 36.9574V22.6393"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M5.37668 22.1772V21.6886H5.87904"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M6.86078 21.6886H29.9161"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M30.407 21.6886H30.9094V22.1772"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M30.9094 23.0999V37.4194"
        stroke="white"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <g filter="url(#filter3_d_55_3596)">
        <path
          d="M26.8719 26.7573H33.4499C33.9881 26.7573 34.4244 27.1816 34.4244 27.7051V32.3522C34.4244 32.8756 33.9881 33.3 33.4499 33.3H26.8719C26.3336 33.3 25.8973 32.8756 25.8973 32.3522V27.7051C25.8973 27.1816 26.3336 26.7573 26.8719 26.7573Z"
          fill="#028C7E"
        />
      </g>
      <path
        d="M30.7142 30.028C30.7142 29.1388 29.9721 28.4171 29.0578 28.4171C28.1435 28.4171 27.4015 29.1388 27.4015 30.028C27.4015 30.9172 28.1435 31.6389 29.0578 31.6389C29.9721 31.6389 30.7142 30.9172 30.7142 30.028Z"
        fill="white"
      />
      <defs>
        <filter
          id="filter0_d_55_3596"
          x="-25.3584"
          y="5.58798"
          width="63.8548"
          height="79.3589"
          filterUnits="userSpaceOnUse"
          color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="1" dy="1" />
          <feGaussianBlur stdDeviation="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_55_3596"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_55_3596"
            result="shape"
          />
        </filter>
        <filter
          id="filter1_d_55_3596"
          x="-30.4937"
          y="4.38168"
          width="68.8466"
          height="64.3087"
          filterUnits="userSpaceOnUse"
          color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="1" dy="1" />
          <feGaussianBlur stdDeviation="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_55_3596"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_55_3596"
            result="shape"
          />
        </filter>
        <filter
          id="filter2_d_55_3596"
          x="1.95816"
          y="18.6199"
          width="34.3697"
          height="24.819"
          filterUnits="userSpaceOnUse"
          color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="1" dy="1" />
          <feGaussianBlur stdDeviation="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_55_3596"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_55_3596"
            result="shape"
          />
        </filter>
        <filter
          id="filter3_d_55_3596"
          x="24.8973"
          y="25.7573"
          width="12.5272"
          height="10.5428"
          filterUnits="userSpaceOnUse"
          color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="1" dy="1" />
          <feGaussianBlur stdDeviation="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_55_3596"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_55_3596"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
};

export default Money;
