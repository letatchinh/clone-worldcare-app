import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import styleStore from '../styles'
import LogoWC from '../assets/images/logoWC.svg';
import Couple from '../assets/images/header/couple.svg';
import TextLink from '../components/TextCustom/TextLink';
import TextStrong from '../components/TextCustom/TextStrong';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import { HOME_SCREEN, LOGIN_SCREEN, REGISTER_SCREEN } from '../constants/screen';
import { Button } from '@rneui/themed';
import { colorPrimary } from '../styles/variable';
import LayoutNoAuth from '../components/layout/LayoutNoAuth';
export default function WelcomeScreen({ navigation }) {


    const onNavigateRegister = () => {
        navigation.navigate(REGISTER_SCREEN);
    };
    const onNavigateHome = () => {
        navigation.navigate(HOME_SCREEN);
    };
    const onNavigateLogin = () => {
        navigation.navigate(LOGIN_SCREEN);
    }
    return (
        <LayoutNoAuth>

            <View style={style.couple}>
            </View>

            <LogoWC width={200} height={200} />

            <TextStrong style={{ textAlign: 'center', fontSize: 13 }}>Chào mừng bạn đến với WorldCareVN</TextStrong>

            <TextLink style={style.haveAccount}>Bạn đã có tài khoản chưa?</TextLink>

            <ButtonLinearGradientPrimary onPress={onNavigateLogin} style={styleStore.CommonStyles.marginTop_30}>Đăng nhập</ButtonLinearGradientPrimary>

            <Button onPress={onNavigateRegister}
                type='outline'
                size='md'
                buttonStyle={style.btn_register}
            >
                <TextLink style={style.textRegister}>Đăng ký</TextLink>
            </Button>
            <ButtonLinearGradientPrimary styleContainer={style.home} type='secondary' onPress={onNavigateHome}
            >
                Trang chủ
            </ButtonLinearGradientPrimary>
        </LayoutNoAuth>
    )
}


const style = StyleSheet.create({
    container : {
        ...styleStore.CommonStyles.container,
        backgroundColor :'white',
        padding : 50
    },
    btn_register: {
        paddingVertical: 15,
        borderRadius: 30,
        borderColor: colorPrimary,
        marginTop: 20,
        minWidth: '100%',


    },
    textRegister: {
        fontWeight: '500'
    },
    haveAccount: {
        marginVertical: 10,
    },
    home: {
        marginTop: 20
    },
    couple : {
        position : 'absolute',
        left : 50,
        top : 50
    }
});