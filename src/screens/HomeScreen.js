import React, { useEffect } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import Body from '../components/layout/Body';
import { setAxiosToken } from '../core/axios';
import { useToken } from '../hook/auth';
const widthScreen = Dimensions.get('screen').width;

export default function HomeScreen({}) {
  const token = useToken();
  useEffect(() => {
    setAxiosToken(token)
  },[token])
  return (
      <View>
        <Body>
        </Body>
      </View>

  )
}

const styles = StyleSheet.create({
  title: {
    position: 'absolute',
    top: '30%',
    left: 20
  },
  text: {
    color: 'white',
    fontSize: widthScreen / 23,
    fontWeight: 'bold'
  }
})