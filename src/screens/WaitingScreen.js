import { Button, Image } from '@rneui/themed';
import { get } from 'lodash';
import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import Waiting from '../assets/images/Waiting.svg';
import ModalCustom from '../components/custom/ModalCustom';
import LayoutNoAuth from '../components/layout/LayoutNoAuth';
import TextPrimary from '../components/TextCustom/TextPrimary';
import TextTitle from '../components/TextCustom/TextTitle';
import { useCheckVersion } from '../hook/system';
import CommonStyles from '../styles/common';
import { colorPrimary } from '../styles/variable';
export default function WaitingScreen({ }) {
  const { statusVersion, isVisible, setIsVisible, onGoHome, onUpdate } = useCheckVersion();
  const onCloseUpdate = useCallback(() => {
    setIsVisible(false);
  }, []);

  return (
    <LayoutNoAuth >
      <View style={CommonStyles.container}>
        <Waiting />
      </View>
      <ModalCustom canClose={false} isFullScreen={true} onCancel={onCloseUpdate} isVisible={isVisible} >
        <Image style={{ width: 200, height: 200 }} resizeMode='cover' source={require('../assets/images/header/newUpdate.jpeg')} />
        <TextTitle textAlign={'center'}>Đã có phiên bản mới</TextTitle>
        <TextPrimary textAlign={'center'}>{!!get(statusVersion, 'isForceUpgrade') ? "Vui lòng cập nhật để sử dụng" : ""}</TextPrimary>
        <Button buttonStyle={styles.buttonUpdate} onPress={() => {
          onCloseUpdate();
          onUpdate();
        }} >
          Cập nhật ngay
        </Button>

        {!get(statusVersion, 'isForceUpgrade') && <Button type='outline' titleStyle={{ color: colorPrimary }} buttonStyle={styles.buttonUpdateLater} onPress={() => {
          onCloseUpdate();
          setTimeout(() => onGoHome(), 200)
        }} >
          Cập nhật sau
        </Button>}
      </ModalCustom>
    </LayoutNoAuth>
  )
}

const styles = StyleSheet.create({
  buttonUpdate: {
    marginTop: 20,
    backgroundColor: colorPrimary,
    paddingVertical: 10,
    borderRadius: 30,
    paddingHorizontal: 30
  },
  buttonUpdateLater: {
    marginTop: 20,
    paddingVertical: 10,
    borderRadius: 30,
    paddingHorizontal: 30,
    borderColor: colorPrimary,
  },
})