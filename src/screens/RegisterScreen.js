import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LogoWC from '../assets/images/logoWC.svg';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import InputBase from '../components/FormComponents/InputBase';
import InputPassword from '../components/InputPassword';
import { LOGIN_SCREEN } from '../constants/screen';
import styleStore from '../styles';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { colorPrimary } from '../styles/variable';
import { CheckBox } from '@rneui/base';
import TextLink from '../components/TextCustom/TextLink';
import LayoutNoAuth from '../components/layout/LayoutNoAuth';

const RegisterScreen = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [agreePolicy,setAgreePolicy] = useState(false);

  const handleUsernameChange = text => {
    setUsername(text);
  };

  const handlePasswordChange = text => {
    setPassword(text);
  };

  const handleRegister = () => {
    // You can implement your authentication logic here
    // For now, let's just log the username and password
    console.log('Username:', username);
    console.log('Password:', password);
  };
  const handleAgreeChange = () => {
    setAgreePolicy(!agreePolicy);
  }
  const onNavigateLogin = () => {
    navigation.navigate(LOGIN_SCREEN)
  }
  return (
    <LayoutNoAuth >
     <LogoWC width={200} height={200}/>
     <InputBase
        InputLeftElement={<AntIcon name="user" size={18} color={colorPrimary}/>}
        value={username}
        placeholder={"Tên"}
        onChangeText={handleUsernameChange}
      />
     <InputBase
        InputLeftElement={<AntIcon name="mail" size={18} color={colorPrimary}/>}
        value={username}
        placeholder={"Số điện thoại/Email"}
        onChangeText={handleUsernameChange}
      />
      <InputPassword
        password={password}
        handlePasswordChange={handlePasswordChange}
      />
      <CheckBox right
           checked={agreePolicy}
           onPress={handleAgreeChange}
           iconType="material-community"
           checkedIcon="checkbox-outline"
           uncheckedIcon={'checkbox-blank-outline'}
           title={  <Text style={styles.agreePolicy}>Tôi đồng ý với <TextLink style={{ fontSize: 12 }}>Chính sách và điều khoản dịch vụ</TextLink></Text>}
         />
      <ButtonLinearGradientPrimary disabled={!agreePolicy}>
      Đăng ký
      </ButtonLinearGradientPrimary>
      <Text style={styles.haveAccount}>Bạn đã có tài khoản ? <Text onPress={onNavigateLogin} style={{...styleStore.TextStyles.link,...styleStore.TextStyles.strong}}>Đăng nhập</Text></Text>
    </LayoutNoAuth>
  );
};
const styles = StyleSheet.create({
  agreePolicy: {
    fontWeight: '500',
    fontSize: 12,
    color : 'black'
  },
  haveAccount:{
    color : 'black'
  }
})

export default RegisterScreen;
