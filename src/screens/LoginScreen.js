import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { StyleSheet, Text, View } from 'react-native';
import AntIcon from 'react-native-vector-icons/AntDesign';
import LogoWC from '../assets/images/logoWC.svg';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import InputBase from '../components/FormComponents/InputBase';
import InputPassword from '../components/InputPassword';
import { HOME_SCREEN, REGISTER_SCREEN, WELCOME_SCREEN } from '../constants/screen';
import { MAIN_DRAWER } from '../constants/screen';
import { useLogin } from '../hook/auth';
import { validateLogin } from '../hook/validateForm';
import styleStore from '../styles';
import { colorPrimary } from '../styles/variable';
import { formatPhoneV2 } from '../utils/helper';
import {get} from 'lodash';
import LayoutNoAuth from '../components/layout/LayoutNoAuth';
const LoginScreen = () => {
  const navigation = useNavigation(); // Access the navigation object
  const { handleSubmit, control, formState: { errors } } = useForm({
    resolver: validateLogin,
  });
  const [isLoading, onLogin] = useLogin(
    () => {
      const {routes,index} = navigation.getState();
      const PreScreen = get(routes,`[${index - 1}].name`);
      if(PreScreen === WELCOME_SCREEN){
        navigation.replace(MAIN_DRAWER);
      }else{
        navigation.goBack();
      }
    }
  );




  const handleLogin = (values) => {
    const { login, password } = values;
    const formatLogin = formatPhoneV2(login);
    onLogin({ login: formatLogin, password })
  };
  const onNavigateRegister = () => {
    navigation.navigate(REGISTER_SCREEN)
  }
  return (


    <LayoutNoAuth >
      <LogoWC width={200} height={200} />
      <Controller
        name='login'
        control={control}
        render={({ field: { value, onChange } }) => <InputBase
          InputLeftElement={<AntIcon name="user" size={18} color={colorPrimary} />}
          value={value}
          placeholder={"Số điện thoại/Email"}
          onChangeText={onChange}
          name='login'
          errors={errors}
        />}
      />
      <Controller
        name='password'
        control={control}
        render={({ field: { value, onChange } }) => <InputPassword
          password={value}
          handlePasswordChange={onChange}
          name='password'
          errors={errors}
        />}
      />


      <ButtonLinearGradientPrimary loading={isLoading} onPress={handleSubmit(handleLogin)} >
        Đăng nhập
      </ButtonLinearGradientPrimary>
      <Text style={styles.notHaveAccount}>Bạn chưa có tài khoản ? <Text onPress={onNavigateRegister} style={{ ...styleStore.TextStyles.link, ...styleStore.TextStyles.strong }}>Đăng ký</Text></Text>
    </LayoutNoAuth>
  );
};

const styles = StyleSheet.create({

  notHaveAccount: {
    marginTop: 20,
    color: 'black'
  }
})

export default LoginScreen;
