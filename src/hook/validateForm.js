import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

const reqString = (mess) => yup.string().required(mess || "Vui lòng nhập");
const reqNumber = (mess) => yup.number().required(mess || "Vui lòng nhập");
const validatePhoneNumber = (mess) => reqString().matches(/^[0-9]{10,10}$/, mess || 'Vui lòng nhập đúng định dạng');
const validateAddress = () => ({
  address: yup.object({
    cityId: reqString("Vui lòng chọn thành phố"),
    districtId: reqString("Vui lòng chọn Quận/Huyện"),
    wardId: reqString("Vui lòng chọn Phường/Xã"),
    street: reqString("Vui lòng nhập tên đường"),
  }),
})
export const validateInfoUser = yupResolver(yup.object({
  fullName: reqString(),
  phoneNumber: reqString(),
  ...validateAddress(),
  idNumber: yup.string().matches(/^[a-zA-Z0-9]{9,15}$/, 'Vui lòng nhập đúng định dạng'), // CMND,
  phoneNumber: validatePhoneNumber(),
}));

export const validateLogin = yupResolver(yup.object({
  login: reqString(),
  password: reqString(),
}));

export const validateWhBillItem = yupResolver(yup.object({
  extraData : yup.object({
    startDate : reqString("Vui lòng chọn ngày bắt đầu"),
    endDate : reqString("Vui lòng chọn ngày kết thúc"),
  })
}));

export const validateAddAddress = yupResolver(yup.object({
  fullName: reqString("Vui lòng nhập tên"),
  phoneNumber: validatePhoneNumber("Vui lòng nhập số điện thoại"),
  ...validateAddress(),
}));