import moment from 'moment';

class DateTime {
  static instance;
  
  constructor(){
    const currentDate = moment();
    this.date = currentDate.date();
    this.month = currentDate.month() + 1;
    this.year = currentDate.year();
  }
  static getInstance() {     
        console.log("INSTANCE");
    if (!DateTime.instance) {
        DateTime.instance = new DateTime();
    }

    return DateTime.instance;
}
  getRangeDate(startDate, endDate,format='YYYY-MM-DD'){
    let rangeDate = [];
    if(startDate && endDate){
        const diff = moment(endDate).diff(moment(startDate),'d');
        for (let index = 0; index <= diff; index++) {
          const dateCurrent = moment(startDate).add(index,'d').format(format);
          rangeDate.push(dateCurrent);
        }
      }
      return rangeDate;
  }

  getFirstAndLastDateAtMonth({month=this.month,date = this.date,year = this.year,format='YYYY-MM-DD',dateString}){
    const fullDate = `${year}-${month}-${date}`;
    const startDate = moment(dateString ?? fullDate,format).startOf('month').format(format);
    const endDate =  moment(dateString ?? fullDate,format).endOf('month').format(format);
    return {startDate, endDate};
  }

  setEndOfDate (d) {
    return moment(d).endOf('day').format("YYYY-MM-DD HH:mm:ss")
  }
    
  setStartOfDate (d) {
    return moment(d).startOf('day').format("YYYY-MM-DD HH:mm:ss")
  }
}

export default DateTime.getInstance();
