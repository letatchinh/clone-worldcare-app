import { get } from 'lodash';
import { useSelector } from "react-redux";
import { actions } from "../../redux/auth/reducer";
import { getAsyncStore, useFailed, useFetchByParam, useSubmit, useSuccess } from "../utils";

const getSelector = (key) => (state) => state.auth[key];
const tokenSelector = getSelector('token');
const isLoadingSelector = getSelector('isLoading');
const profileSelector = getSelector('profile');
const loginFailedSelector = getSelector('loginFailed');
const getProfileFailedSelector = getSelector('getProfileFailed');
const isGetProfileLoadingFailedSelector = getSelector('isGetProfileLoading');
const updateProfileFailedSelector = getSelector('updateProfileFailed');
const updateProfileSuccessSelector = getSelector('updateProfileSuccess');
const isUpdateProfileLoadingSelector = getSelector('isUpdateProfileLoading');
export const useLogin = (callback) => {
    useSuccess(tokenSelector,'',callback);
    useFailed(loginFailedSelector,"Sai tài khoản hoặc mật khẩu");
    return useSubmit({
        loadingSelector : isLoadingSelector,
        action : actions.loginRequest,
        
    })
};
export const useLogout = () => {

    return useSubmit({
        loadingSelector : isLoadingSelector,
        action : actions.logoutRequest,
    })
};


export const useToken = () => {
    const token = useSelector(tokenSelector);
    return token
};

// Get New Profile from Sever
export const useProfile = () => {
    const token = useSelector(tokenSelector);
    return useFetchByParam({
        action : actions.getProfileRequest,
        dataSelector : profileSelector,
        failedSelector : getProfileFailedSelector,
        loadingSelector : isGetProfileLoadingFailedSelector,
        param : token,
    })
}

// Get Profile From redux
export const useGetProfile = () => {
    const profile = useSelector(profileSelector);
    return profile
}

export const getTokenFromAsyncStore = async() => {
    const persist = await getAsyncStore("persist:auth");
    return JSON.parse(get(persist,'token') || "");
};

export const useUpdateProfile = (callback) => {
    useSuccess(updateProfileSuccessSelector,'Cập nhật tài khoản thành công',callback);
    useFailed(updateProfileFailedSelector);
    return useSubmit({
        loadingSelector : isUpdateProfileLoadingSelector,
        action : actions.updateProfileRequest,
    });
};

export const useRefreshLinkRefProfile = () => {
    return useSubmit({
        loadingSelector : isUpdateProfileLoadingSelector,
        action : actions.refreshLinkRefRequest,
    });
};

