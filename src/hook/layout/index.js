import { useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux"
import { layoutReducerAction } from "../../redux/layout/reducer";

export const useScroll = () => {
    const scrollEnabled = useSelector(state => state.layout.scrollEnabled);
    const scrollEnabled_ = useMemo(() => scrollEnabled,[scrollEnabled])
    const dispatch = useDispatch();
    const onEnabledScroll = useCallback(() => {
        dispatch(layoutReducerAction.enabledScroll());
    },[dispatch])
    const onDisabledScroll = useCallback(() => {
        dispatch(layoutReducerAction.disabledScroll());
    },[])
    return {
        scrollEnabled:scrollEnabled_,
        onEnabledScroll,
        onDisabledScroll,
    }
}