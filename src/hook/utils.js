import AsyncStorage from '@react-native-async-storage/async-storage';

import moment from "moment";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isInteger } from 'lodash';
import { Toast } from 'react-native-toast-message/lib/src/Toast';
import { useNavigation } from '@react-navigation/native';
import { get } from 'lodash'
export const setAsyncStore = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // saving error
  }
};
export const getAsyncStore = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
  }
};
export async function clearAsyncStorage() {
  try {
    await AsyncStorage.clear();
  } catch (error) {
    console.error('Error clearing async storage:', error);
  }
}



export const useSuccess = (successSelector, mess, onSuccess) => {
  const success = useSelector(successSelector);
  useEffect(() => {
    if (success) {
      if (mess) {
        // Display Toast Here
        Toast.show({
          type: 'success',
          text1: mess,
          position: 'top'
        })
      }

      if (onSuccess) onSuccess(success);
    }
  }, [success, mess]);
};

export const useFailed = (failedSelector, mess, onFailed, mute = false) => {
  const failed = useSelector(failedSelector);
  useEffect(() => {
    if (failed && !mute) {
      // Display Toast Here
      Toast.show({
        type: 'error',
        text1: mess || failed?.response?.data?.message || 'Có lỗi gì đó xảy ra!',
        position: 'top'
      })
    }

    if (onFailed) onFailed(failed);
  }, [failed, mess, onFailed]);
};

export const useFetchByParam = (props) => {
  const {
    action,
    dataSelector,
    failedSelector,
    loadingSelector,
    param,
    muteOnFailed,
    actionUpdate,
    conditionRun = true,
  } = props;
  const dispatch = useDispatch();
  const data = useSelector(dataSelector);
  const isLoading = useSelector(loadingSelector);
  useEffect(() => {
    if (param && conditionRun) dispatch(action(param));
  }, [dispatch, action, param,conditionRun]);

  useFailed(failedSelector, undefined, undefined, muteOnFailed);

  const useUpdateData = (dataUpdate) => { // Update Data To Redux
    if (actionUpdate && typeof actionUpdate === 'function') {
      dispatch(actionUpdate(dataUpdate));
    }
  }
  return [data, isLoading, useUpdateData];
};


export const useFetch = (props) => {
  const { action, dataSelector, failedSelector, loadingSelector, dependency } = props;
  const dispatch = useDispatch();
  const data = useSelector(dataSelector);
  const isLoading = useSelector(loadingSelector);
  const dependency_ = useMemo(() => dependency, dependency)

  const [reRender, setReRender] = useState(false);
  useEffect(() => {
    setReRender(!reRender);
  }, [dependency_]);

  useEffect(() => {
    dispatch(action());
  }, [dispatch, action, reRender]);

  useFailed(failedSelector);

  return [data, isLoading];
};

export const useSubmit = ({ loadingSelector, action }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(loadingSelector);

  const handleSubmit = (values) => {
    dispatch(action(values));
  };

  return [isLoading, handleSubmit];
};


export const useResetState = (resetAction) => {
  const dispatch = useDispatch();
  useEffect(() => {
    return () => {
      dispatch(resetAction());
    };
  }, [dispatch, resetAction]);
};

export const getSelectors = (moduleName) => {
  const getSelector = (key) => (state) => state[moduleName][key];

  return {
    loadingSelector: getSelector('isLoading'),
    listSelector: getSelector('list'),
    getListFailedSelector: getSelector('getListFailed'),

    getByIdLoadingSelector: getSelector('isGetByIdLoading'),
    getByIdSelector: getSelector('byId'),
    getByIdFailedSelector: getSelector('getByIdFailed'),

    deleteSuccessSelector: getSelector('deleteSuccess'),
    deleteFailedSelector: getSelector('deleteFailed'),

    isSubmitLoadingSelector: getSelector('isSubmitLoading'),
    createSuccessSelector: getSelector('createSuccess'),
    createFailedSelector: getSelector('createFailed'),

    updateSuccessSelector: getSelector('updateSuccess'),
    updateFailedSelector: getSelector('updateFailed'),
    pagingSelector: getSelector('paging')
  };
};

export const vietnamMoment = (v, formatTime) => {
  if (v) {
    const utcMoment = moment.utc(v);
    if (formatTime) {
      return utcMoment.format(formatTime);
    }
    else {
      return utcMoment
    }
  }
  return null

}

export const formatter = (num, fixed = 2,delimiter = ',') => {
  if (!num) return 0
  const parsedNum = parseFloat(num);
  if (isInteger(parsedNum) && parsedNum === 0) {
    return num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
  } else if (Number.isInteger(parsedNum)) {
    return parsedNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
  } else {
    return parsedNum.toFixed(fixed).toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
  }
};


export const useMergeState = initialState => {
  const [state, setState] = useState(initialState);
  const setMergedState = newState =>
    setState(prevState => ({ ...prevState, ...newState }));
  return [state, setMergedState];
};

export const useResetStack = (tab) => {
  if (!tab) return;
  const { reset, getParent, canGoBack } = useNavigation();
  useEffect(() => {
    getParent()
      .addListener('tabPress', (e) => {
        if (canGoBack()) {
          reset({
            index: 0,
            routes: [{ name: tab }]
          })
        }
      });

    return () => {
      getParent()
        .removeListener('tabPress', (e) => {
          if (canGoBack()) {
            reset({
              index: 0,
              routes: [{ name: tab }]
            })
          }
        });
    }

  }, [])
};

export const useLoadMore = (paging, setQuery) => {
  if (!paging) return;
  const { hasNextPage, nextPage } = paging;
  const onNext = () => {
    if (hasNextPage) {
      setQuery({ page: nextPage });
    }
  };
  return onNext
}

export const useFetchState = ({ request, query, useDocsInResponse = true, init = [], fieldGet,reFetch,nullNotFetch = false }) => {
  const [data, setData] = useState(init);
  const [loading, setLoading] = useState(false);
  const req = useCallback(request, []);
  const fetch = useCallback(async () => {
    try {
      setLoading(true);
      const response = await req(query);
      if (fieldGet) {
        setData(get(response, fieldGet))
      } else {
        if (useDocsInResponse) {
          setData(get(response, 'docs', []));
        } else {
          setData(response);
        }
      }

      setLoading(false);
    } catch (error) {
      console.log(error,'error');
      setLoading(false);
    }
  }, [query,reFetch])
  useEffect(() => {
    if(nullNotFetch){
      !!query && fetch();
    }else{
      fetch()
    }
  }, [fetch,nullNotFetch,query]);
  const dataReturn = useMemo(() => data, [data])
  return [dataReturn, loading]
}