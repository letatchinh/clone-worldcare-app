import { useSelector } from "react-redux";
import api from "../../api";
import { geoActions } from "../../redux/geo/reducer";
import { useFailed, useFetch, useFetchByParam, useResetState, useSubmit, useSuccess } from "../utils";


const getSelector = key => state => state.geo[key];

const areasLoadingSelector = getSelector('isAreasLoading');
const areasSelector = getSelector('areas');
const getAreasFailedSelector = getSelector('getAreasFailed');

const citiesLoadingSelector = getSelector('isCitiesLoading');
const citiesSelector = getSelector('cities');
const getCitiesFailedSelector = getSelector('getCitiesFailed');

const districtsLoadingSelector = getSelector('isDistrictsLoading');
const districtsSelector = getSelector('districts');
const getDistrictsFailedSelector = getSelector('getDistrictsFailed');

const wardsLoadingSelector = getSelector('isWardsLoading');
const wardsSelector = getSelector('wards');
const getWardsFailedSelector = getSelector('getWardsFailed');

const isLoadingAddAddressSelector = getSelector('isLoadingAddAddress');

const addAddressSuccessSelector = getSelector('addAddressSuccess');
const addAddressFailedSelector = getSelector('addAddressFailed');

const updateAddressSuccessSelector = getSelector('updateAddressSuccess');
const updateAddressFailedSelector = getSelector('updateAddressFailed');

const deleteAddressSuccessSelector = getSelector('deleteAddressSuccess');
const deleteAddressFailedSelector = getSelector('deleteAddressFailed');

const isLoadingGetAddressSelector = getSelector('isLoadingGetAddress');
const addressSelector = getSelector('address');
const getAddressFailedSelector = getSelector('getAddressFailed');

export const useAreas = () =>
  useFetch({
    action: geoActions.getAreasRequest,
    loadingSelector: areasLoadingSelector,
    dataSelector: areasSelector,
    failedSelector: getAreasFailedSelector
  });

export const useCities = () =>
  useFetch({
    action: geoActions.getCitiesRequest,
    loadingSelector: citiesLoadingSelector,
    dataSelector: citiesSelector,
    failedSelector: getCitiesFailedSelector
  });
export const useDistricts = cityCode =>
  useFetchByParam({
    action: geoActions.getDistrictsRequest,
    loadingSelector: districtsLoadingSelector,
    dataSelector: districtsSelector,
    failedSelector: getDistrictsFailedSelector,
    param: cityCode
  });

export const useWards = districtCode =>
  useFetchByParam({
    action: geoActions.getWardsRequest,
    loadingSelector: wardsLoadingSelector,
    dataSelector: wardsSelector,
    failedSelector: getWardsFailedSelector,
    param: districtCode
  });

export const getMyAddress = async(callback) => {
  const address = await api.cart.getMyAddresses();
  if(callback && typeof callback === 'function') {
    callback(address);
  }
  return address;
};

export const useAddAddress = (callback) => {
  useSuccess(addAddressSuccessSelector,'Thêm địa chỉ thành công',callback);
  useFailed(addAddressFailedSelector);
  return useSubmit({
      loadingSelector : isLoadingAddAddressSelector,
      action : geoActions.addAddressRequest,
  });
};


export const useUpdateAddress = (callback) => {
  useSuccess(updateAddressSuccessSelector,'Cập nhật địa chỉ thành công',callback);
  useFailed(updateAddressFailedSelector);
  return useSubmit({
      loadingSelector : isLoadingAddAddressSelector,
      action : geoActions.updateAddressRequest,
  });
};

export const useDeleteAddress = () => {
  useSuccess(deleteAddressSuccessSelector,'Xoá địa chỉ thành công');
  useFailed(deleteAddressFailedSelector);
  return useSubmit({
      loadingSelector : isLoadingAddAddressSelector,
      action : geoActions.deleteAddressRequest,
  });
};


export const useResetAddress = () => {
  return useResetState(geoActions.resetAddressAction)
}

export const useAddress = () => {

  const addAddressSuccess = useSelector(addAddressSuccessSelector);
  const updateAddressSuccess = useSelector(updateAddressSuccessSelector);
  
  return useFetch({
    action: geoActions.getAddressRequest,
    loadingSelector: isLoadingGetAddressSelector,
    dataSelector: addressSelector,
    failedSelector: getAddressFailedSelector,
    dependency : [addAddressSuccess,updateAddressSuccess]
})
}