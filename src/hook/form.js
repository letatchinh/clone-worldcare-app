
export const useSetForm = (setValue) => {
    return (data) => {
        for (const [key, value] of Object.entries(data)) {
            setValue(key, value)
        }
    }
}