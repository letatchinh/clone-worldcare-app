import { useNavigation } from '@react-navigation/native';
import { get } from 'lodash';
import { useCallback, useEffect, useState } from 'react';
import { Linking, Platform } from 'react-native';
import { getVersion } from 'react-native-device-info';

import api from '../../api';
import { HOME_SCREEN } from '../../constants/screen';
import { MAIN_DRAWER } from '../../constants/screen';
export const useCheckVersion = () => {
    const [statusVersion,setStatusVersion] = useState();
    const [isVisible,setIsVisible] = useState(false);
    const navigation = useNavigation();
    const onGoHome = useCallback(() => navigation.replace(MAIN_DRAWER),[])

    useEffect(() => {
        const fetch = async() => {
                const version = getVersion();
                const isIos = Platform.OS === 'ios';
                const query = {
                    key : isIos ? "VER_IOS" : "VER_ANDROID",
                    deviceVersion : version
                }
            const response = await api.system.checkVersion(query);
            setStatusVersion(response);
            // If Have Version Higher Will Show Modal Update Version , Else Goto Home
            if(!!get(response,'isHigh')){
                setIsVisible(true);
            }else{
                onGoHome()
            }
        };
        fetch();
    },[]);

    const onUpdate = () => {
        const isIos = Platform.OS === 'ios';
        if(isIos) {
          const link = 'itms-apps://apps.apple.com/id/app/WorldCareVN/6470611906?l=id';
          Linking.canOpenURL(link).then(supported => {
            supported && Linking.openURL(link);
          }, (err) => console.log(err));
        }else{
          Linking.openURL("market://details?id=com.worldcarevn");
        }
      }
    return {statusVersion,isVisible,setIsVisible,onGoHome,onUpdate};
}