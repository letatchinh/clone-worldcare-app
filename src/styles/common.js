import {StyleSheet} from 'react-native';
import { borderRadiusPrimary, colorPrimary } from './variable';

const CommonStyles = StyleSheet.create({
    // Flex
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height:'100%',
        padding:10,
    },
    flexCenter:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    bg_white:{
        backgroundColor : 'white'
    },
    gap_5:{
        gap : 5,
    },
    gap_10:{
        gap : 10,
    },
    gap_15:{
        gap : 15,
    },
    gap_20:{
        gap : 20,
    },
    // Padding
    padding_5:{
        padding : 5,
    },
    padding_10:{
        padding : 10,
    },
    padding_15:{
        padding : 15,
    },
    padding_20:{
        padding : 20,
    },
    padding_50:{
        padding : 50,
    },
    // Margin
    margin_5:{
        margin : 5,
    },
    margin_10:{
        margin : 10,
    },
    margin_15:{
        margin : 15,
    },
    margin_20:{
        margin : 20,
    },
    // Margin TOP
    marginTop_5:{
        marginTop : 5,
    },
    marginTop_10:{
        marginTop : 10,
    },
    marginTop_15:{
        marginTop : 15,
    },
    marginTop_20:{
        marginTop : 20,
    },
    marginTop_30:{
        marginTop : 30,
    },
    // Margin Bottom
    marginBottom_5:{
        marginBottom : 5,
    },
    marginBottom_10:{
        marginBottom : 10,
    },
    marginBottom_15:{
        marginBottom : 15,
    },
    marginBottom_20:{
        marginBottom : 20,
    },
    // Border Radius
    borderRadius_5:{
        borderRadius : 5,
    },
    borderRadius_10:{
        borderRadius : 10,
    },
    borderRadius_15:{
        borderRadius : 15,
    },
    borderRadius_20:{
        borderRadius : 20,
    },
    borderRadius_Primary:{
        borderRadius : borderRadiusPrimary,
    },
    // width
    width_100:{
        width: '100%',
    },
    wrapCircles:{
        borderColor : colorPrimary,
        borderWidth : 2,
    },
    fontFamily:{
        fontFamily : 'Gill Sans'
    },
  });
export default CommonStyles