import {StyleSheet} from 'react-native';

const InputStyles = StyleSheet.create({
    input: {
      width: '100%',
      marginBottom: 10,
      paddingLeft: 20,
      // paddingTop : 20,
      // paddingBottom : 20,
      borderWidth: 0,
      backgroundColor: 'white',
      borderRadius: 8,
      position : 'relative',
    },
    bgPrimary:{
        backgroundColor: '#F2F7FF',
        borderColor:'#79A8FF',
        borderWidth : 1,
        // color:'#79A8FF',
    }
  });

  export default InputStyles