import CommonStyles from "./common";
import InputStyles from "./input";
import TextStyles from "./text";
import HeaderStyles from "./header";
import OrderStatus from "./orderStatus";

export default {
    CommonStyles,
    InputStyles,
    TextStyles,
    HeaderStyles,
    OrderStatus,
}