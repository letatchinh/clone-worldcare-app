export const colorPrimaryLowest = '#F2F7FF'; // blue Lowest
export const colorPrimaryLower = '#ACD6FF'; // blue Lower
export const colorPrimaryLow = '#71A6FF'; // blue Low
export const colorPrimary = '#3481FF'; // blue Medium
export const colorPrimaryBold = '#2E2C80'; // blue Bold
export const colorSecondary = '#34D2B9'; // green Lower banana
export const borderRadiusPrimary = 32;
export const colorBoxShadow = '#000'; // gray Shadow
export const colorBorderGray = '#ECECED'; // gray 
export const colorDisabledText = '#A1A4AC'; // Disabled Text
export const colorSuccess = '#53C41A'; // Success

export const colorError = '#F75555' ;// red Error
export const backgroundColorPrimaryBold = '#E9F0FF'; // blue Low
export const backgroundColorPrimary = '#F2F7FF'; // blue Low
export const backgroundColorPrimaryLow = '#E8EFFA'; // blue Lower
export const paddingBody = 10;

export const disabledColorBackground = '#9B9999';

