import {StyleSheet} from 'react-native';
import {WH_BILL_ITEM_STATUS, WH_BILL_STATUS} from '../constants/defaultValue'
import { colorPrimary, colorPrimaryBold } from './variable';
const OrderStatus = StyleSheet.create({
    [WH_BILL_STATUS.NEW] : {
        color : colorPrimaryBold,
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#f0f0f0',
    },
    [WH_BILL_STATUS.PREPAYMENT_CONFIRMED] : {
        color : colorPrimaryBold,
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#f0f0f0',
    },
    [WH_BILL_ITEM_STATUS.CONFIRMED] : {
        color : colorPrimaryBold,
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#f0f0f0',
    },
    [WH_BILL_STATUS.IN_PROGRESS] : {
        color : '#1890ff',
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#e6f7ff',
    },
    [WH_BILL_STATUS.COMPLETED] : {
        color : 'white',
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#52c41a',
    },
    [WH_BILL_STATUS.CANCELLED] : {
        color : 'white',
        paddingHorizontal : 10,
        paddingVertical : 7,
        backgroundColor : '#f5222d',
    },
    [WH_BILL_STATUS.CUSTOMER_CANCEL] : {
          color : '#717171',
         paddingHorizontal : 10,
         paddingVertical : 7,
         backgroundColor : '#f0f0f0',
    },
  });

  export default OrderStatus