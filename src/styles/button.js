import {StyleSheet} from 'react-native';

const ButtonStyles = StyleSheet.create({
    primary: {
     borderRadius : 32,
    },
    bgPrimary:{
        backgroundColor: '#E7F0FF',
    }
  });

  export default ButtonStyles