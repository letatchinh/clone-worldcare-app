import axios from "axios";
import { API_URL } from "../../constants/defaultValue";

export const axiosToken = axios.create({
    baseURL: API_URL,
    headers: {
      'Accept-Language' : 'vi',
      'Content-Type': 'application/json',
    }
  });
  export function setAxiosToken(token) {
    axiosToken.defaults.headers.common.Authorization = 'Bearer ' + token;
  }
  
  export function removeAxiosToken() {
    axiosToken.defaults.headers.common.Authorization = '';
  }

