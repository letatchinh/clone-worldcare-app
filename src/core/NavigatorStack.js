import { NavigationContainer } from "@react-navigation/native";
import React from 'react';
import MainStack from "./stackNavigator/MainStack";
import './LocaleRNCalendarConfig'; // Config Calendar

export default function NavigatorStack() {
  return (
    <NavigationContainer >
      <MainStack />
    </NavigationContainer>
  );
}