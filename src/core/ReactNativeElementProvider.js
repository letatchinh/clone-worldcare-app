import { ThemeProvider } from '@rneui/themed';
import React from 'react';

export default function ReactNativeElementProvider({ children, ...props }) {

    return (
        <ThemeProvider {...props}>
            {children}
        </ThemeProvider>
    )
}
