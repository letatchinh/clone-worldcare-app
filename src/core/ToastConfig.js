import { ErrorToast,SuccessToast } from "react-native-toast-message";

const toastConfig = {
    success: (props) => (
      <SuccessToast
        {...props}
        text1NumberOfLines={2}
        text2NumberOfLines={2}
        
      />
    ),
    error: (props) => (
      <ErrorToast
        {...props}
        text1NumberOfLines={2}
        text2NumberOfLines={2}
      />
    ),
  };

  export default toastConfig