import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import WrapperStack from '../../components/layout/WrapperStack';
import { MAIN_DRAWER } from '../../constants/screen';
import { authScreen } from '../../constants/allScreen';
import WaitingScreen from '../../screens/WaitingScreen';
import fontFamily from '../../assets/fonts/fontFamily';
import { colorPrimary } from '../../styles/variable';
import DrawerNavigation from '../DrawerNavigation';
const Stack = createNativeStackNavigator();
export default function MainStack() {
    return (
        <Stack.Navigator screenOptions={{
            headerTitleAlign: 'center',
            headerTitleStyle: {
              fontFamily: fontFamily.roboto,
              color : colorPrimary,
              fontSize : 16
            }
        
          }}>
            <Stack.Screen
                key={'WAITING'}
                name={'WAITING'}
                component={WaitingScreen}
                options={{ headerShown: false }}
            />
            {/* HomePage */}
            <Stack.Screen name={MAIN_DRAWER} options={{ headerShown: false,title : "Chào mừng" }} component={DrawerNavigation} />

            {/* Login Screen */}
            {authScreen?.map(({ name, component: Component, options = {} }) => (
                <Stack.Screen
                    key={name}
                    name={name}
                    options={options}
                >
                    {(props) => <WrapperStack navigation={props.navigation} Component={Component} />}
                </Stack.Screen>
            ))}
        </Stack.Navigator>
    )
}
