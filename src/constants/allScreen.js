import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import * as Screens from './screen';

export const allScreen = [
  {
    name: Screens.HOME_SCREEN,
    component: HomeScreen,
    options: {
      headerShown: false,
      title: '',
    },
  },

];


export const authScreen = [
  {
    name: Screens.LOGIN_SCREEN,
    component: LoginScreen,
    options: {title: 'Đăng nhập'},
    isAuth: false,
  },
  {
    name: Screens.REGISTER_SCREEN,
    component: RegisterScreen,
    options: {title: 'Đăng ký'},
    isAuth: false,
  },
  {
    name: Screens.WELCOME_SCREEN,
    component: WelcomeScreen,
    options: {title: 'Chào mừng'},
    isAuth: false,
  },
];

