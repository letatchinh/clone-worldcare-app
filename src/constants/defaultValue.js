export const API_URL = 'https://api.congtyso.com';

export const GENDER = {
    M: { vi: 'Nam', en: 'Male' },
    F: { vi: 'Nữ', en: 'Female' }
  };

  export const WH_SERVICE_TYPESHOW = {
    TIMES:'TIMES',
    MINUTES:'MINUTES'
  }
  export const WH_SERVICE_TYPESHOW_VI = {
    TIMES:'Lần',
    MINUTES:'Phút'
  }

  export const WH_BILL_STATUS_VI = {
    NEW: 'Mới',
    PREPAYMENT_CONFIRMED: 'Xác nhận cọc',
    IN_PROGRESS: 'Đang thực hiện',
    CANCELLED: 'Đã hủy',
    CUSTOMER_CANCEL: 'Đã chấm dứt',
    COMPLETED : 'Đã hoàn thành'
  };
  
  export const WH_BILL_STATUS = {
    NEW: 'NEW',
    PREPAYMENT_CONFIRMED: 'PREPAYMENT_CONFIRMED',
    IN_PROGRESS: 'IN_PROGRESS',
    CANCELLED: 'CANCELLED',
    CUSTOMER_CANCEL: 'CUSTOMER_CANCEL',
    COMPLETED: 'COMPLETED',
  };

  export const WH_BILL_ITEM_STATUS = {
    NEW: 'NEW',
    CONFIRMED: 'CONFIRMED',
    IN_PROGRESS: 'IN_PROGRESS',
    COMPLETED: 'COMPLETED',
    CANCELLED: 'CANCELLED',
  };

  export const WH_BILL_ITEM_STATUS_VI = {
    NEW: 'Mới tạo',
    CONFIRMED: 'Đã xác nhận',
    IN_PROGRESS: 'Đang thực hiện',
    COMPLETED: 'Đã hoàn thành',
    CANCELLED: 'Đã huỷ',
  };

  export const WH_APPOINTMENT_STATUS = {
    NEW: 'NEW',
    IN_PROGRESS: 'IN_PROGRESS',
    COMPLETED: 'COMPLETED',
    CANCELLED: 'CANCELLED',
  };

  export const WH_APPOINTMENT_STATUS_VI = {
    NEW: 'Mới',
    IN_PROGRESS: 'Đang thực hiện',
    COMPLETED: 'Đã hoàn thành',
    CANCELLED: 'Đã huỷ',
  };

  export const WH_APPOINTMENT_STATUS_COLOR = {
    NEW: '#717171',
    IN_PROGRESS: '#3481ff',
    COMPLETED: '#35d2b9',
    CANCELLED: '#ff1e1e',
  };
  
  export const APPOINTMENT_STATUS = {
    CREATED: 'CREATED',
    CONFIRMED: 'CONFIRMED',
    CANCEL: 'CANCEL',
    ARRIVED: 'ARRIVED',
  };

  export const APPOINTMENT_STATUS_VI = {
    CREATED: 'Mới',
    CONFIRMED: 'Đã xác nhận',
    CANCEL: 'Đã huỷ',
    ARRIVED : "Đã hoàn thành"
  };

  export const APPOINTMENT_STATUS_COLOR = {
    CREATED: '#717171',
    CONFIRMED: '#3481ff',
    ARRIVED: '#35d2b9',
    CANCEL: '#ff1e1e',
  };

  export const SERVICES_NAME = {
    DOCTOR_AT_HOME: 'Điều dưỡng tại nhà',
    CLINIC_APPOINTMENT: 'Khám tại phòng khám',
    MEDICAL_TEST: 'Xét nghiệm'
  };

  export const MAX_LIMIT = 200;