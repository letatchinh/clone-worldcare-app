import requester from './requester';

export default {
  postSignIn: data => requester.post('/api/v1/authenticate', data),
  postSignInWithPhone: data => requester.post('/api/v1/auth/sms-login', data),
  postSignUp: data => requester.post('/api/v1/register', data),
  postResetPassword: data => requester.put('/api/v1/customer-account/reset-password', data),
  checkPhone: data => requester.post('/api/v1/customer-account/check-phone', data),
  getProfile: () => requester.getWithToken('/api/v1/profile'),
  postProfile: data => requester.putWithToken('/api/v1/profile', data),
  getRefs: ({id,...data}) => requester.getWithToken(`/api/v1/customer-account-parent-child/${id}`,data),

};
