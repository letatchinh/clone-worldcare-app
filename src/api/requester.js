import axios from 'axios';
import { API_URL } from '../constants/defaultValue';
import { axiosToken } from '../core/axios';
import { getTokenFromAsyncStore } from '../hook/auth';
import { convertQueryString } from '../utils/helper';

const responseBody = (res) => res.data;

export const getBaseURL = () => {
  return typeof window === 'undefined'
    ? process.env.API_URL || API_URL
    : API_URL;
};
export default {
  getOutJson: async (url, locale = 'vi') => {
    const response = await fetch(`${API_URL}${url}`, {
      headers: {
        'Accept-Language': locale,
      },
    });
    return await response.json();
  },

  get: async (url,params={},locale = 'vi') => axios.get(`${getBaseURL()}${url}${convertQueryString(params)}`, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
      },
    }).then(responseBody)
  
  ,

  post: (url, data = {}, locale = 'vi') =>
    axios.post(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
      },
    }).then(responseBody)
  ,

  put: (url, data = {}, locale = 'vi') =>
    axios.put(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
      },
    }).then(responseBody)
  ,
  postFormData: async (url, data) => {
    axios.post(`${getBaseURL()}${url}`, data).then(responseBody)
  },
  getWithToken: async (url, params = {},locale = 'vi') => axiosToken.get(`${getBaseURL()}${url}${convertQueryString(params)}`).then(responseBody),
  postWithToken: async (url, data = {}, locale = 'vi') =>  axiosToken.post(`${getBaseURL()}${url}`, data).then(responseBody),
  putWithToken: async (url, data = {}, locale = 'vi') =>  axiosToken.put(`${getBaseURL()}${url}`, data).then(responseBody),
  deleteWithToken: async (url, data = {}, locale = 'vi') =>  axiosToken.delete(`${getBaseURL()}${url}`, {data}).then(responseBody)
  ,
};
