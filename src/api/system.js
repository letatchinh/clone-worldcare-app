import requester from "./requester";

export default {
    checkVersion : (data) => requester.post('/api/v1/system/check',data),
}