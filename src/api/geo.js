import requester from './requester';

const geo = {
  getAreas: () => requester.get('/api/v1/area'),
  getCities: () => requester.get('/api/v1/city'),
  getDistricts: cityCode => requester.get(`/api/v1/city/${cityCode}/district`),
  getWards: districtCode => requester.get(`/api/v1/district/${districtCode}/ward`),
  addAddress: data => requester.postWithToken('/api/v1/me/address', data),
  updateAddress: data => requester.putWithToken(`/api/v1/me/address/${data._id}`, data),
  deleteAddress: id => requester.deleteWithToken(`/api/v1/me/address/${id}`),
};

export default geo;
