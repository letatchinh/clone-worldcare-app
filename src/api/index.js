import auth from './auth';
import geo from './geo';
import system from './system';

export default {
    auth,
    geo,
    system,
}