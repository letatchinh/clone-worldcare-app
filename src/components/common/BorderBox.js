import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { colorDisabledText, colorPrimary } from '../../styles/variable';
import { Button } from '@rneui/themed';

const BorderBox = ({disabled = false,onPress = () => {},...props}) => {
  return (
    <Button onPress={onPress} containerStyle={{...styles.container,...disabled && {borderColor : colorDisabledText}}} type='clear' buttonStyle={{
        ...styles.buttonStyle,
        
    }}>
      {props.children}
    </Button>
  )
}

export default BorderBox;

const styles = StyleSheet.create({
    buttonStyle : {
        
        flexDirection : 'column',
        alignItems :'flex-start'
    },
    container : {
        width : '100%',
        padding : 10,
        borderWidth : 1,
        borderColor : colorPrimary,
        
        borderRadius : 10,
    }
})