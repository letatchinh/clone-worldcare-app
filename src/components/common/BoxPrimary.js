import React from 'react';
import { StyleSheet, View } from 'react-native';
import { backgroundColorPrimary } from '../../styles/variable';

const BoxPrimary = ({ children, noPadding = false, style }) => {
  return (
    <View style={
      {
        ...styles.container,
        ...noPadding && { paddingHorizontal: 0, paddingVertical: 0 },
        ...style
      }
    }>
      {children}
    </View>
  )
}

export default BoxPrimary;

const styles = StyleSheet.create({
  container: {
    backgroundColor: backgroundColorPrimary,
    paddingHorizontal: 25,
    paddingVertical: 7,
    borderRadius: 10,
    width: '100%',
  }
})