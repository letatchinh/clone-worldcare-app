import React from 'react'
import { ActivityIndicator } from 'react-native'
import Stack from './Stack'

const Loading = () => {
  return (
        <Stack center>
          <ActivityIndicator size="small" color="#0000ff" />
        </Stack>
  )
}

export default Loading