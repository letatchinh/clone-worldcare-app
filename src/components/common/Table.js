import { View, Text, StyleSheet, ActivityIndicator } from 'react-native'
import React, { useState } from 'react'
import TextPrimary from '../TextCustom/TextPrimary'
import { colorBorderGray, colorPrimary } from '../../styles/variable'
import { get } from 'lodash';
import Empty from './Empty';
const ColHeader = ({ children, setWidths, index, isLast }) => {
    const onLayout = (event) => {
        const { width } = event.nativeEvent.layout;
        setWidths(pre => ({ ...pre, [index]: width }))
    }
    return <View onLayout={onLayout} style={{
        ...styles.containerTitleHeader, ...!isLast && {
            borderRightColor: 'white',
            borderRightWidth: 1
        }
    }}>
        <TextPrimary style={styles.titleHeader} textAlign={'center'}>{children}</TextPrimary>
    </View>
};

const Row = ({ children }) => <View style={styles.rowBody}>
    {children}
</View>

const RowItem = ({ children, width, align }) => <View style={{ ...styles.rowItem, width, alignItems: align }}>{children}</View>

const Table = ({ dataSource = [], columns = [], isLoading, title }) => {
    const [widths, setWidths] = useState({});
    return (
        !!dataSource?.length ?
            <View style={styles.container}>
                {isLoading && <View style={styles.loading}>
                <ActivityIndicator />
                </View>}
                <View style={styles.title}>
                    {title && title}
                </View>
                <View style={styles.header}>
                    {columns?.map(({ title,key }, index) => <ColHeader key={key} isLast={index === columns?.length - 1} index={index} setWidths={setWidths}>{title}</ColHeader>)}
                </View>
                <View style={styles.body}>
                    {dataSource?.map((item, index) =>
                        <Row key={index}>
                            {columns?.map(({ dataIndex, render, align }, i) => <RowItem align={align} width={widths[i]}>{render(get(item, dataIndex, ''), item, index)}</RowItem>)}
                        </Row>
                    )}

                </View>
            </View> : 
            <View style={styles.container}>
                <Empty />
                <TextPrimary>Trong</TextPrimary>
            </View>
    )
}

export default Table;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        marginVertical : 10
    },
    loading : {
        ...StyleSheet.absoluteFill,
        backgroundColor : '#ffffff4a',
        zIndex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },
    title: {

    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        borderColor: colorPrimary,
        borderWidth: 1,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: colorPrimary
    },
    containerTitleHeader: {

        flexGrow: 1,
    },
    titleHeader: {
        color: 'white',

    },
    body: {

    },
    rowBody: {
        flexDirection: 'row',
        // justifyContent: 'space-evenly',
        width: '100%',
        paddingBottom : 5,
        borderBottomColor: colorPrimary,
        borderBottomWidth: 1
    },
    rowItem: {
    }
})

