import { View, Text, StyleSheet, Animated } from 'react-native'
import { useEffect, useRef } from 'react'
import EmptyData from '../../assets/images/booking/EmptyData.svg';
import TextPrimary from '../TextCustom/TextPrimary';

const Empty = ({title}) => {
    const opacity = useRef(new Animated.Value(0)).current;

    const show = () => {
        // Will change fadeAnim value to 1 in 5 seconds
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }).start();
      };
    
      useEffect(() => {
        show();
      },[]);
  return (
    <Animated.View style={{
        ...styles.container,
        opacity
    }}>
     <EmptyData />
     {title && <TextPrimary>{title}</TextPrimary>}
    </Animated.View>
  )
}

export default Empty;

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        width : '100%',
        height : '100%',
        marginVertical : 20
    }
})