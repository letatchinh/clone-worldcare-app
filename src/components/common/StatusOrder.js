import { View } from 'react-native'
import React from 'react'
import OrderStatus from '../../styles/orderStatus'
import TextPrimary from '../TextCustom/TextPrimary'

const StatusOrder = ({status,text}) => {
  return (
    <View style={{
        borderRadius: 100 / 2,
        overflow: 'hidden',
    }}>
        <TextPrimary style={{
            ...OrderStatus[status],
            textAlign : 'center',

        }}>{text}</TextPrimary>
    </View>
  )
}

export default StatusOrder