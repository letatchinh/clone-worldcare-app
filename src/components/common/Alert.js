import { View, StyleSheet } from 'react-native'
import React from 'react'

const typeExport = {
    default : {
        backgroundColor : 'white'
    },
    success : {
        backgroundColor : '#CBF0DD'
    },
    info : {
        backgroundColor : '#BDE1FF'
    },
    warning : {
        backgroundColor : '#FEF1DA'
    },
    error : {
        backgroundColor : '#FEEBE2'
    },
}
/**
 * Test component for displaying text.
 * @component
 * @param {Object} props - The properties of the component.
 * @param {("default"|"success"|"info"|"warning"|"error")} props.type
 * @param {Object} props.style
 * @returns {JSX.Element} - Rendered component.
 */

const Alert = ({type = 'default',children,style,...props}) => {
  return (
    <View style={{
        ...styles.container,
        ...typeExport[type],
        ...style
    }}
    {...props}
    >
      {children}
    </View>
  )
}

export default Alert;

const styles = StyleSheet.create({
    container : {
        paddingVertical : 7,
        paddingHorizontal : 26,
        borderRadius : 10,
        width : '100%',
    }
})