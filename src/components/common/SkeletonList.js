import { Skeleton } from '@rneui/themed';
import { range } from 'lodash';
import React from 'react';
import { StyleSheet, View } from 'react-native';
const SkeletonList = ({ count = 6 }) => {
    return (
        <View style={styles.list}>
            {range(0,count)?.map((item) => <View key={item} style={styles.item}>
                <Skeleton width={'100%'} height={60} />
                <Skeleton width={'100%'} height={10} />
            </View>)}
        </View>
    )
}

export default SkeletonList

const styles = StyleSheet.create({
    item: {
        display: 'flex',
        alignItems: 'center',
        width: '31%',
        gap : 5, 
        borderRadius: 5,
        flexGrow: 1,
    },
    list: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        gap: 10,
    }
})