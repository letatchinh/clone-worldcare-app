import React from 'react'
import { StyleSheet, View } from 'react-native';

/**
 * Test component for displaying text.
 * @component
 * @param {Object} props - The properties of the component.
 * @param {Boolean} props.fullWidth 
 * @param {Boolean} props.center 
 * @param {Number} props.gap 
 * @param {Number} props.mt 
 * @param {Number} props.mb 
 * @param {Boolean} props.wrap 
 * @param {Object} props.style 
 * @param {("row"|"row-reverse"|"column"|"column-reverse")} props.dir 
 * @param {("center"|"flex-end"|"flex-start"|"space-around"|"space-between"|"space-evenly")} props.justify - The type of the text. 
 * @param {("baseline"|"flex-end"|"flex-start"|"center"|"stretch")} props.align - The type of the text. 
 * 
 * @returns {JSX.Element} - Rendered component.
 */

export default function Stack({children,fullWidth,center,justify='flex-start',align='stretch',dir='row',gap = 0,mt,mb,style,wrap,...props}) {
  return (
    <View style={{
        ...styles.container,
        justifyContent:justify,
        alignItems:align,
        ...center && {justifyContent : 'center',alignItems : 'center'},
        flexDirection : dir,
        gap ,
        ...fullWidth && {width : '100%'},
        ...mt && {marginTop : mt},
        ...mb && {marginBottom : mb},
        ...wrap && {flexWrap : 'wrap'},
        ...style
    }}
    {...props}
    >
        {children}
    </View>
  )
}

const styles = StyleSheet.create({
    container : {
        display :'flex',
    }
})