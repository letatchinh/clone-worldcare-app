import { View, Text, useWindowDimensions } from 'react-native';
import React, { memo } from 'react';
import RenderHtml from 'react-native-render-html';
import { colorPrimaryBold } from '../../styles/variable';


const RenderHtmlInRN = ({html}) => {
    const {width: contentWidth} = useWindowDimensions();
  return (
    <RenderHtml
      contentWidth={contentWidth}
      source={{html}}
      baseStyle={{color : colorPrimaryBold, flex : 1}}
    />
  )
}

export default memo(RenderHtmlInRN)