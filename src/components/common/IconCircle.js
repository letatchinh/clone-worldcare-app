import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { backgroundColorPrimary } from '../../styles/variable';

const IconCircle = ({icon}) => {
  return (
    <View style={styles.icon}>
            {icon}
        </View>
  )
}

export default IconCircle;

const styles = StyleSheet.create({
    icon : {
        borderRadius : 100/2,
        backgroundColor  : '#407CE221',
        padding : 10,
    }
})