import { useRoute } from '@react-navigation/native';
import React, { useMemo } from 'react';
import {get} from 'lodash';
import { ScrollView, StatusBar, StyleSheet } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useResetStack } from '../../hook/utils';
import { colorPrimary } from '../../styles/variable';
import { useScroll } from '../../hook/layout';
const stickyHeaderCart = {
  stickyHeaderHiddenOnScroll: false,// Use 'false' to show the sticky header
  stickyHeaderIndices: [0],  // use First child as sticky header (Cart)
}

const LayoutScroll = ({ children, removeHeader = false, isAuth, tab, removeWrapScroll = false, backgroundColorCart = colorPrimary,backgroundColor }) => {
  const insets = useSafeAreaInsets()
  useResetStack(tab);
const ref = React.useRef(null);

const scrollToTop = () => {
  if (ref.current) {
    ref.current.scrollTo({ y: 0, animated: true });
  }
};
  // Pass additional props to children
  const childrenWithProps = React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        scrollToTop,
        // Add more props as needed
      });
    }
    return child;
  });
  const {scrollEnabled} = useScroll();
  return (
      <SafeAreaView
        style={{ paddingBottom: -35, flex: 1, paddingTop: -get(insets,'top',20) }}
        {...!removeHeader && { edges: ['right', 'bottom', 'left'] }}
      >
        <StatusBar translucent backgroundColor="transparent" barStyle={'dark-content'} />

        {removeWrapScroll
          ?
          childrenWithProps
          : <ScrollView
          ref={ref}
          scrollEnabled={scrollEnabled}
            style={{
              ...styles.container,
              backgroundColor : backgroundColor || 'white'
            }}
            {...isShowCart && stickyHeaderCart}
          >
            {childrenWithProps}
          </ScrollView>
        }

      </SafeAreaView>
  )
}

export default LayoutScroll;

const styles = StyleSheet.create({
  container: {
  }
})
