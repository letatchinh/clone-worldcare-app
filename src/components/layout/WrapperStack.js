import React from 'react'
import LayoutFull from './LayoutFull'

export default function WrapperStack({ Component, navigation }) {
  return <LayoutFull>
    <Component navigation={navigation} />
  </LayoutFull>
}
