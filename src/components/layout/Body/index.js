import React from 'react';
import { Dimensions, View } from 'react-native';
let ScreenHeight = Dimensions.get("window").height;

const Body = ({ children, bgColor = 'white',style }) => {
  return (
    <View style={{
      padding: 10,
      display: 'flex',
      gap: 20,
      alignItems: 'center',
      backgroundColor: bgColor,
      ...style
    }}>
      {children}
    </View>
  )
}

export default Body