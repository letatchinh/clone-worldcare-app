import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import CommonStyles from '../../styles/common';
import Couple from '../../assets/images/header/couple.svg';
import Plug from '../../assets/images/header/plug-out-line.svg';
import PlugLarge from '../../assets/images/header/plug-outline-large.svg';
const LayoutNoAuth = ({ children }) => {
  return (
    <View style={styles.container}>
      <View style={styles.couple}>
        <Couple />
      </View>
      <View style={styles.plugRightTop}>
        <Plug />
      </View>
      <View style={styles.plugLeftBottom}>
        <PlugLarge />
      </View>
      {children}

    </View>
  )
}

export default LayoutNoAuth;

const styles = StyleSheet.create({
  container: {
    ...CommonStyles.container,
    backgroundColor: 'white',
    padding: 20,
    justifyContent : 'flex-start'
  },

  couple: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  plugRightTop: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  plugLeftBottom: {
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
});