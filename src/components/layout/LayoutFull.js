import React from 'react';
import { ScrollView, View } from 'react-native';
import styleStore from '../../styles';
import SafeAreaViewFull from '../SafeAreaViewFull';

export default function LayoutFull({children}) {
  return (
    <SafeAreaViewFull>
      <ScrollView
      style={{flex : 1}}
      contentContainerStyle={{flex : 1}}
        >
        {children}
      </ScrollView>
    </SafeAreaViewFull>
  )
}
