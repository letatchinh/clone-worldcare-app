import React from 'react'
import IonIcon from 'react-native-vector-icons/Ionicons';
import { colorPrimary } from '../../../../styles/variable';

export default function RightButton() {
  return (
    <IonIcon name='notifications' color={colorPrimary} size={24}/>
  )
}
