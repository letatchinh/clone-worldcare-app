import { useNavigation } from '@react-navigation/native';
import { Avatar } from '@rneui/themed'
import React from 'react'
import { TouchableOpacity } from 'react-native'
import { PROFILE_SCREEN } from '../../../../constants/screen';
export default function LeftButton() {
    const navigation = useNavigation(); // Access the navigation object
    const onNavigateProfile = () => {
        navigation.navigate(PROFILE_SCREEN)
      }
    return (
        <TouchableOpacity onPress={onNavigateProfile}>
            <Avatar
                size={32}
                rounded
                source={{ uri: "https://randomuser.me/api/portraits/men/36.jpg" }}
            />
        </TouchableOpacity>
    )
}
