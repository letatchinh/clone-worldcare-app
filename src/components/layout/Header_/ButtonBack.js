import { useNavigation } from '@react-navigation/native';
import React from 'react'
import {TouchableOpacity} from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';

export default function ButtonBack() {
    const navigation = useNavigation(); // Access the navigation object
    const onGoBack = () => {
        navigation.goBack();
    }
    return (
        <TouchableOpacity onPress={onGoBack}>
            <EntypoIcon name='chevron-left' size={24} />
        </TouchableOpacity> // Remove the back button
    )
}
