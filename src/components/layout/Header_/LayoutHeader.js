import React from 'react';
import styles from '../../../styles';
import Stack from '../../common/Stack';

export default function LayoutHeader({children}) {
  return (
    <Stack style={styles.HeaderStyles.row} justify='space-between' align={'center'}>
        {children}
    </Stack>
  )
};

