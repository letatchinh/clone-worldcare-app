
import { Button } from '@rneui/themed';
import React from 'react'
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import stylesCommon from '../../styles/common';
import { colorPrimary } from '../../styles/variable';

const colorLinearActivePrimary = ['#3481ff', '#103393'];
const colorLinearActiveSecondary = ['#3A7CFF', '#35D2B9'];
const colorLinearActiveDanger = ['#9c0505', '#f96666'];
const colorLinearInActive = ['white', 'white'];
const typeBtn = {
    primary : {
        color : colorLinearActivePrimary,
    },
    secondary : {
        color : colorLinearActiveSecondary,
        start : { x: 0, y: 0 },
        end : { x: 1, y: 0 }
    },
    danger : {
        color : colorLinearActiveDanger,
        start : { x: 1, y: 1 },
        end :   { x : 0, y : 0.5}
    },
}
export default function ButtonLinearGradientPrimary({ children, style, onPress, disabled = false, loading = false, active = true, styleContainer,width = '100%',type = 'primary' }) {
    return (
        <Button
            disabled={disabled || loading}
            onPress={onPress}
            loading={loading}
            containerStyle={{
                width
            }}
            buttonStyle={
                {
                    width: '100%',
                    paddingHorizontal: 20,
                    paddingVertical: 18.5,
                    borderRadius: 32,
                    ...styleContainer
                }
            }
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
                colors: disabled ? ['#D4D4D8', '#D4D4D8'] : active ? typeBtn[type].color : colorLinearInActive,
                style: {
                    ...styles.linearGradient,
                    ...!active && {
                        borderColor: colorPrimary,
                        borderWidth: 1,
                    }, 
                    ...style
                },
                ...typeBtn[type].start && {start:typeBtn[type].start},
                ...typeBtn[type].end && {end:typeBtn[type].end},
            }}
        >
            <Text style={active ? styles.textActive : styles.textInActive}>
                {children}
            </Text>
        </Button>
    )
}

var styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 15,
        borderRadius: 32,
        width: '100%',

        ...stylesCommon.flexCenter
    },
    textActive: {
        color: 'white',
        fontSize: 14,
        fontWeight: '700',
    },
    textInActive: {
        color: colorPrimary,
        fontSize: 14,
        fontWeight: '700',
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
});