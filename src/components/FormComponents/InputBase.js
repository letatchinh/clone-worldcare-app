import { TextInput, View } from 'react-native'
import styleStore from '../../styles'
import Stack from '../common/Stack'
import ErrorMessage from './ErrorMessage'

export default function InputBase({value,style,placeholder,onChangeText,secureTextEntry=false,isRequired=false,InputLeftElement,InputRightElement,errors,name,...props}) {
  return (
    <View style={{width : '100%'}}>
      <Stack style={{...styleStore.InputStyles.input,...styleStore.InputStyles.bgPrimary,...style}} align={'center'}>
    {InputLeftElement && InputLeftElement}
      <TextInput
        style={{fontSize : 14,marginLeft : 10,paddingVertical : 20,color : 'black',width : '100%'}}
        value={value}
        placeholder={placeholder}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        placeholderTextColor='#79A8FF'
        {...props}
      />
      
        {InputRightElement && InputRightElement}
    </Stack>
    <ErrorMessage errors={errors} name={name}/>
    </View>
  )
}
