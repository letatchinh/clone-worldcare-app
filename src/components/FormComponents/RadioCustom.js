import { Button } from '@rneui/themed';
import React, { useState } from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { colorPrimary } from '../../styles/variable';

const RadioCustom = ({ value, onChange,onPress }) => {
    return (
        <Button onPress={onPress} type='clear'>
            {
                value
                    ? <AntDesign name='checkcircle' size={18} color={colorPrimary} />
                    : <Entypo name='circle' size={18} color={colorPrimary} />
            }
        </Button>


    )
}

export default RadioCustom