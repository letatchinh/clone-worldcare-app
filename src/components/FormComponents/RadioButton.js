import { get, head } from 'lodash';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ButtonLinearGradientPrimary from './ButtonLinearGradientPrimary';
const RadioButtonGroup = ({value, options,onChange = () => {},defaultFirstActive,styleButton,styleContainer,justify = 'flex-start',styleText,Component = ButtonLinearGradientPrimary,propComponent,styleTextActive,styleTextInActive }) => {
  return (
    <View style={{
      ...styles.container,
      justifyContent : justify,
      gap : 10
    }}>
      {options?.map(({ label, value: valueOption }) =>
        <Component
          {...propComponent}
          onPress={() => onChange(valueOption)}
          active={value ? valueOption === value : (defaultFirstActive ? get(head(options),'value') === valueOption : false)}
          style={
            {
            ...styles.button,
            ...styleButton
            }
          }
          styleContainer={{
            ...styles.buttonContainer,
            ...styleContainer
          }}
          key={valueOption}>
          <Text style={{...styles.text,...styleText,...valueOption === value  ? styleTextActive : styleTextInActive}}>{label}</Text>
        </Component>)}
    </View>

  )
}

export default RadioButtonGroup;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap : 'wrap',
    gap : 20,
    width : '100%',
    
    // gap: 10,
    // justifyContent : 'space-between'
  },
  buttonContainer: {
    width: 'auto',
  },
  button: {
    paddingHorizontal: 1,
    paddingVertical: 10
  },
  text: {
    fontSize: 13,
  }
})

