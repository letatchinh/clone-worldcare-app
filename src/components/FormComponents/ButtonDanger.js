import { Button } from '@rneui/base'
import React from 'react'
import { StyleSheet } from 'react-native'
import CommonStyles from '../../styles/common'

export default function ButtonDanger({children,onPress,style,...props}) {
  return (
    <Button 
    onPress={onPress} 
    buttonStyle={{...styles.button,...style}} 
    color='error'
    {...props} 
    >{children}</Button>
  )
}

const styles = StyleSheet.create({
    button:{
        ...CommonStyles.flexCenter,
        minWidth : '100%',
    },  
})
