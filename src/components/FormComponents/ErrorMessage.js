import React, { useMemo } from 'react'
import TextPrimary from '../TextCustom/TextPrimary'
import {get} from 'lodash';
const ErrorMessage = ({errors,name}) => {
    const msg = useMemo(() => get(errors,`${name}.message`,''),[errors,name]);
  return (
    msg ? <TextPrimary size='sm' type='danger'>{msg}</TextPrimary> : null
  )
}

export default ErrorMessage