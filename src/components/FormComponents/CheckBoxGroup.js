import { View, Text, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import Stack from '../common/Stack';
import { CheckBox, Icon } from '@rneui/themed';
import { colorPrimary } from '../../styles/variable';

/**
 * Test component for displaying text.
 * @component
 * @param {Object} props - The properties of the component.
 * @param {"horizontal"|"vertical"} props.direction 
 * @param {"checkbox"|"button"} props.type 
 * @param {Object} props.containerCheckBox 
 * @param {Object} props.containerCheckBoxChecked 
 * @param {Object} props.containerCheckBoxUnChecked 
 * @param {Object} props.titleCheckBoxChecked 
 * @param {Object} props.titleCheckBoxUnChecked 
 * @param {Object} props.style 
 * @param {Object} props.titleStyle 
 * @param {Boolean} props.singleSelect 
 * @param {Array} props.options 
 * @param {Function} props.onChange 
 * @param {Function} props.ComponentLabel 
 * @returns {JSX.Element} - Rendered component.
 */
const noneIcon = {
    checkedIcon:
        <Icon
            name="radio-button-checked"
            type="material"
            color="white"
            size={0}
            iconStyle={{ display: 'none' }}
        />,
    uncheckedIcon:
        <Icon
            name="radio-button-checked"
            type="material"
            color="white"
            size={0}
            iconStyle={{ display: 'none' }}
        />,


}
const CheckBoxGroup = ({ options = [], onChange = () => {}, value: value_, direction = 'horizontal', type = 'checkbox',singleSelect,style,containerCheckBox,containerCheckBoxChecked,containerCheckBoxUnChecked,titleCheckBoxChecked,titleCheckBoxUnChecked,titleStyle,ComponentLabel }) => {
    const [values, setValues] = useState({});
    // Init Values
    useEffect(() => {
        let initValues = {};
        console.log(options,'options');
        options.map(({ value }) => Object.assign(initValues, { [value]: false }));
        setValues(initValues);
    }, [options]);

    // set Default Value if Have
    useEffect(() => {
        if (value_ && !Array.isArray(value_)) {
            return console.error("Value Phải là Array")
        };
        if(!value_.length){
            return 
        }
        let newValues = {};
        if (!!Object.keys(values)?.length) {
            for (const [key, v] of Object.entries(values)) {
                if(value_?.includes(key)){
                    newValues[key] = true;
                }else{
                    newValues[key] = false;
                }
                
            }
        } else {
            options.map(({ value }) => Object.assign(newValues, { [value]: false }));
        }
        value_?.forEach(v => {
            if(options?.some(({value : v_}) => v_ === v)){
                newValues[v] = true
            }
        });

        setValues(newValues);
    }, [value_]);

    const onToggle = (value) => {
        const checked = values[value];
        let newValues = {};
        if(singleSelect){
            for (const [key, v] of Object.entries(values)) {
                if(String(key) === String(value)){
                    newValues[key] = !checked
                }else{
                    newValues[key] = false;
                }
              }
        }else{
             newValues = { ...values, [value]: !checked };
        }
        const newValuesToArray = Object.keys(newValues)?.filter(key => !!newValues[key]);
            setValues(newValues);
            onChange(newValuesToArray);
    }
    return (
        <Stack style={style} fullWidth wrap gap={10} align={direction === 'vertical' ? 'flex-start' : 'center'} dir={direction === 'horizontal' ? 'row' : 'column'}>
            {options?.map(({ label, value,...props }) => {
                const checked = values[value];
                return <CheckBox
                    {...type === 'button' && noneIcon}
                    containerStyle={{
                        ...styles.containerCheckBox,
                        ...checked ? styles.containerChecked : styles.containerUnChecked,
                        ...checked ? containerCheckBoxChecked : containerCheckBoxUnChecked,
                        ...containerCheckBox,
                        
                    }}
                    textStyle={{
                        ...checked
                            ? styles.labelChecked
                            : styles.labelUnChecked,
                        ...checked
                            ? titleCheckBoxChecked
                            : titleCheckBoxUnChecked,
                        ...titleStyle
                    }}
                    onPress={() => onToggle(value)}
                    center
                    title={ComponentLabel ? <ComponentLabel {...props} onPress={() => onToggle(value)} checked={checked} label={label}/> :label}
                    checked={checked} />
            })}
        </Stack>
    )
}

export default CheckBoxGroup;

const styles = StyleSheet.create({
    containerCheckBox: {
        borderRadius: 20,
        width: 'auto'
    },
    containerChecked: {
        backgroundColor: colorPrimary
    },
    containerUnChecked: {

    },
    labelChecked: {
        color: '#fff'
    },
    labelUnChecked: {

    },
})