import { TextInput } from 'react-native'
import React, { useEffect, useState } from 'react'
import { formatter } from '../../hook/utils';

const Number = ({ value, onChange, useFormatter = true, separator = ',', ...props }) => {
    const [text, setText] = useState();
    const onChange_ = (val) => {
        if (useFormatter) {
            const removeSeparator = val?.replaceAll(separator, '');
            onChange(removeSeparator)
        } else {
            onChange(val);
        }
    }
    useEffect(() => {
        if (useFormatter) {
            if (!value) {
                setText('')
            } else {
                const textFormatter = formatter(value);
                setText(textFormatter);
            }
        } else {
            setText(text)
        }
    }, [value])
    return (
        <TextInput {...props} inputMode='numeric' value={text} onChangeText={onChange_} />
    )
}

export default {
    Number,
}