import { View, Text } from 'react-native'
import React from 'react'
import { Switch } from '@rneui/themed'

const SwitchCustom = ({value,onChange,disabled,...props}) => {
  return (
    <Switch {...props} disabled={disabled} thumbColor={'white'} trackColor={{false : '#DFE3EB' , true : '#1D89DD' }}  ios_backgroundColor="#DFE3EB" value={value} onChange={onChange} />
  )
}

export default SwitchCustom