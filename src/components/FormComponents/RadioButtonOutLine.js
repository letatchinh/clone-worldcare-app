import React from 'react'
import RadioButtonGroup from './RadioButton'
import ButtonPrimary from '../custom/ButtonPrimary'
import { colorPrimary } from '../../styles/variable'

const RadioButtonOutLine = ({value,onChange,options,Component}) => {
  return (
    <RadioButtonGroup
    value={value}
    onChange={(v) => onChange(v)}
    Component={Component ?? ButtonPrimary}
    propComponent={{
      width: '30%',
      justify : 'space-between',
      buttonStyle: {
        borderRadius: 20,
        paddingLeft: 5,
        paddingRight: 5,
        paddingVertical: 10,
      },
      containerStyle: {
        paddingLeft: 5,
        paddingRight: 5,
      }
    }}
    styleText={{
      fontWeight: '700',
    }}
    styleTextActive={{
      color: 'white',
    }}
    styleTextInActive={{
      color: colorPrimary,
    }}
    options={options}

  />
  )
}

export default RadioButtonOutLine