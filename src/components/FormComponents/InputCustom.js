import { Input } from '@rneui/themed';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { colorPrimary, colorPrimaryBold } from '../../styles/variable';
import ErrorMessage from './ErrorMessage';

const typeExport = {
  primary : {
    container : {
      backgroundColor : '#F2F7FF',
    },
    input : {
      color : colorPrimary,
      fontWeight : '700',
    },
  },
  secondary : {
    container : {
      backgroundColor : '#FAFAFA',
      elevation : 2,
    },
    input : {
      color : colorPrimaryBold,
      fontWeight : '700',
    },
  },
}

const InputCustom = ({value,style,placeholder,onChangeText,rightIcon,styleContainer,styleInput,styleInputContainer,leftIcon,errors,name,type='primary',...props}) => {
  return (
      <View>
        <Input 
        value={value}
        placeholder={placeholder || "Vui lòng nhập"}
        onChangeText={onChangeText}
        {...props}
        containerStyle={{
          ...styles.container,
          ...typeExport[type].container,
          ...styleContainer,
          }}
        inputStyle={{
          ...styles.input,
          ...typeExport[type].input,
          ...styleInput
          }}
        inputContainerStyle={{...{borderBottomWidth:0},...styleInputContainer}}
        rightIcon={rightIcon}
        leftIcon={leftIcon}
    />
    <ErrorMessage errors={errors} name={name}/>
      </View>
  )
}

export default InputCustom;

const styles = StyleSheet.create({
    container : {
      paddingHorizontal: 20,
      borderRadius: 15,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',  
    },
    input : {
      borderBottomWidth : 0,
      borderBottomColor: 'transparent', // Set the border color to transparent
      fontSize : 14,
    }
})