import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { colorPrimaryBold } from '../../styles/variable';
import fontFamily from '../../assets/fonts/fontFamily';

const TextTitle = ({children,style,textAlign}) => {
  return (
      <Text style={{
        ...styles.text,
        ...textAlign && {textAlign},
        ...style,
      }}> {children}</Text>
  )
}

export default TextTitle;

const styles = StyleSheet.create({
    text : {
        color : colorPrimaryBold,
        fontWeight : 'bold',
        fontSize : 18,
        fontFamily : fontFamily.roboto
    }
});
