import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { colorDisabledText, colorPrimaryBold } from '../../styles/variable';
import {get} from 'lodash';
import fontFamily from '../../assets/fonts/fontFamily';
const sizeStyle = {
    lg : {
        fontWeight : '800',
        fontSize : 18,
    },
    semiLg : {
        fontWeight : '700',
        fontSize : 16,
    },
    md : {
        fontWeight : '600',
        fontSize : 14,
    },
    sm : {
        fontWeight : '400',
        fontSize : 12,
    },
};

/**
 * Test component for displaying text.
 * @component
 * @param {Object} props - The properties of the component.
 * @param {Object} props.style 
 * @param {Number} props.fs 
 * @param {Number} props.lh 
 * @param {Number} props.numberOfLines 
 * @param {String|Number} props.width
 * @param {String} props.fw 
 * @param {String} props.color 
 * @param {"italic"|"normal"} props.fontStyle 
 * @param {("lg"|"md"|"semiLg"|"sm")} props.size - The size of the text. .
 * @param {("primary"|"danger"|"secondary")} props.type - The type of the text. 
 * @param {("auto"|"left"|"right"|"center"|"justify")} props.textAlign - The type of the text. 
 * @returns {JSX.Element} - Rendered component.
 */

const TextPrimary = ({ children,size='md', style,fs,fw,color,lh,textAlign,width,type='primary',fontStyle,numberOfLines,onPress,...props }) => {
    
    return (
        <Text {...props}
        {...onPress && {onPress}}
        {...numberOfLines && {numberOfLines}}
         style={
            {
                ...get(styles,[type],{}),
                ...styles.defaultStyle, // Default style Or
                ...sizeStyle[size], // choices Size Or
                ...fs && {fontSize : fs},
                ...fw && {fontWeight : fw?.toString()},
                ...lh && {lineHeight : lh},
                ...fontStyle && {fontStyle},
                ...color && {color},
                ...textAlign && {textAlign},
                ...width && {width},
                ...style // Custom style You Can Custom fontWeight,Size, etc...
            } 
        }>
            {children}
        </Text>
    )
}

export default TextPrimary;

const styles = StyleSheet.create({
    defaultStyle: {
        fontWeight: '700',
        padding: 5,
        padding : 1,
        fontFamily: fontFamily.roboto
    },
    primary : {
        color: colorPrimaryBold,
    },
    danger : {
        color : 'red',
    },
    secondary : {
        color : colorDisabledText
    },
});