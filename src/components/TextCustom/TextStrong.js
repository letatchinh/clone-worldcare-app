import React from 'react'
import { Text } from 'react-native'
import styleStore from '../../styles'

export default function TextStrong({children,style}) {
  return (
    <Text style={{...styleStore.TextStyles.primaryBold,...styleStore.TextStyles.strong,...styleStore.TextStyles.fontSize_14,...style}}>{children}</Text>
  )
}
