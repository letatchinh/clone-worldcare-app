import React from 'react'
import { Text } from 'react-native'
import fontFamily from '../../assets/fonts/fontFamily'
import { colorPrimaryBold } from '../../styles/variable'

const Roboto = ({children,color,style,fw}) => {
  return (
      <Text style={{
        fontFamily : fontFamily.roboto,
        color : color || colorPrimaryBold,
        fontWeight : fw || '400',
        ...style
      }}>{children}</Text>
  )
}



export default {Roboto}