import React from 'react'
import { Text } from 'react-native'
import fontFamily from '../../assets/fonts/fontFamily'
import styleStore from '../../styles'

export default function TextLink({children,style,underline,onPress,fs}) {
  return (
    <Text 
    {...onPress && {onPress}}
    style={{
      ...styleStore.TextStyles.primary,
      ...styleStore.TextStyles.link,
      ...styleStore.TextStyles.fontSize_14,
      ...style,
      ...underline && {textDecorationLine: 'underline'},
      ...fs && {fontSize: fs},
      fontFamily : fontFamily.roboto,
      }}>{children}</Text>
  )
}
