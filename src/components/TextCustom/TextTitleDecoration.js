import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import TextTitle from './TextTitle';
import LeftDecoration from '../../assets/images/component/decorationLeft.svg';
import RightDecoration from '../../assets/images/component/decorationRight.svg';
const TextTitleDecoration = ({
  children,
  textStyle,
  colorLeftDecoration = '#3481FF',
  colorRightDecoration = '#35D2B9',
}) => {
  return (
    <View style={styles.container}>
      <TextTitle style={textStyle}>{children}</TextTitle>
      <View style={styles.decorationLeft}>
        <LeftDecoration stroke={colorLeftDecoration} />
      </View>
      <View style={styles.decorationRight}>
        <RightDecoration stroke={colorRightDecoration} />
      </View>
    </View>
  );
};

export default TextTitleDecoration;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  decorationLeft: {
    position: 'absolute',
    left: -35,
    top: 5,
  },
  decorationRight: {
    position: 'absolute',
    right: -50,
    top: -10,
  },
});
