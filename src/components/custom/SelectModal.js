import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import SelectCustom from './SelectCustom'
import AntDesign from 'react-native-vector-icons/AntDesign';
import { colorBorderGray } from '../../styles/variable';

const OutLine = ({onChange,value,errors,name,options,leftIcon,placeholder,listMode = 'MODAL',allowClear = true,onClear = () => {},styleContainer}) => {
    return (
        <SelectCustom
            type={'secondary'}
            listMode={listMode}
            value={value}
            setValue={(callback) => onChange(callback())}
            options={options}
            placeholder={placeholder || "Vui lòng chọn"}
            style={styles.outlineContainer}
            styleContainer={styleContainer}
            stylePlaceholder={styles.outlinePlaceholder}
            ArrowDownIconComponent={<AntDesign name='down' />}
            ArrowUpIconComponent={<AntDesign name='up' />}
            leftIcon={leftIcon}
            name={name}
            errors={errors}
            allowClear={allowClear}
            onClear={onClear}
        />
    )
}

export default { OutLine };

const styles = StyleSheet.create({
    outlineContainer: {
      borderColor: colorBorderGray,
      borderWidth: 1,
      backgroundColor: 'white',
    },
    outlineContainerInput: {
      borderColor: colorBorderGray,
      borderWidth: 1,
      backgroundColor: 'white'
    },
    outlineInput: {
      paddingLeft: 10,
    },
    outlinePlaceholder: {
      color: '#8896A1'
    },
    label: {
      marginBottom: 10
    }
  })