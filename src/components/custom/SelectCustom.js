import { Button, CheckBox } from '@rneui/themed';
import { useCallback, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { colorBorderGray, colorDisabledText, colorPrimary, colorPrimaryBold } from '../../styles/variable';
import { filterOptionSlug } from '../../utils/helper';
import Stack from '../common/Stack';
import ErrorMessage from '../FormComponents/ErrorMessage';
import TextPrimary from '../TextCustom/TextPrimary';
import RenderListItem from './RenderListItem';

const Item = ({label,isSelected,onPress,item,custom,...props}) => {
    const onSelect = useCallback(() => onPress(item, custom),[])
    return <Button buttonStyle={{paddingVertical : 0}} onPress={onSelect} type='clear'>
        <Stack fullWidth style={{paddingRight : 20,paddingLeft : 10}} justify='space-between' align='center'>
        <TextPrimary lh={25} width={'90%'}>{label}</TextPrimary>
        <View style={{width : '10%'}}>
        <CheckBox
           checked={isSelected}
           onPress={onSelect}
           checkedIcon="dot-circle-o"
           uncheckedIcon="circle-o"
         />
        </View>
    </Stack>
    </Button>
}

const typeExport = {
    primary : {
      container : {
        backgroundColor : '#F2F7FF',
      },
      input : {
        color : colorPrimary,
        fontWeight : '700',
      },
    },
    secondary : {
      container : {
        backgroundColor : '#FAFAFA',
        elevation : 2,
      },
      input : {
        color : colorPrimaryBold,
        fontWeight : '700',
      },
    },
  }
// NOTICE : IF WRAP OUTSIDE THE VIEW , please Set Z index to VIEW
// NOTICE : Value,setValue is a useState example : const [value, setValue] = useState(null)
export default function SelectCustom({ 
    value,
    setValue,
    options = [],
    placeholder,
    zIndex = 100,
    listMode = "SCROLLVIEW",
    style,
    styleContainer,
    ArrowDownIconComponent,
    ArrowUpIconComponent,
    leftIcon,
    stylePlaceholder,
    errors,
    name,
    showSearch = true, 
    type = 'primary',
    allowClear,
    onClear = () => {}
}) {
    const [open, setOpen] = useState(false);
    const [option, setOption] = useState([]);
    const [optionSearch, setOptionSearch] = useState();
    const onSearch = (input) => setOptionSearch(option?.filter(option => filterOptionSlug(input, option)))
    useEffect(() => {
        setOption(options);
    }, [options]);
    useEffect(() => {
        setOptionSearch(null);
    },[open])
    return (
        <View style={styleContainer}>
            {leftIcon && <View style={{
                position: 'absolute',
                top: '25%',
                left: 20
            }}>
                {leftIcon}
            </View>}
            <DropDownPicker
                modalProps={{
                    animationType: "fade"
                }}
                disableLocalSearch={true}
                searchable={showSearch}
                searchPlaceholder="Tìm kiếm"
                onChangeSearchText={onSearch}
                zIndex={zIndex}
                listMode={listMode} // use can Replace with MODAL
                open={open}
                value={value}
                items={optionSearch ?? option}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setOption}
                placeholder={placeholder || "Vui lòng chọn"}
                style={{ ...styles.container,  ...typeExport[type].container, ...style, ...leftIcon && { paddingLeft: 60 } }}
                ArrowDownIconComponent={() => ArrowDownIconComponent ?? <AntDesign name='caretdown' color={colorPrimaryBold} size={18} />}
                ArrowUpIconComponent={() => ArrowUpIconComponent ?? <AntDesign name='caretup' color={colorPrimaryBold} size={18} />}
                placeholderStyle={{ ...styles.placeholder, ...stylePlaceholder }}
                textStyle={{...styles.text,  ...typeExport[type].input}}
                listItemContainerStyle={styles.listItemContainer}
                listItemLabelStyle={styles.listItemLabel}
                dropDownContainerStyle={styles.dropDownContainer}
                TickIconComponent={() => <AntDesign name='check' size={20} color={colorPrimary} />}
                closeOnBackPressed={true}
                // renderListItem={(props) => <RenderListItem {...props}/>}
                searchTextInputStyle={styles.searchText}
                searchContainerStyle={styles.searchContainer}
                CloseIconComponent={({style}) => <AntDesign name='close' color={'black'}  size={20} style={{...style}} />}
                
                
            />
            {value && allowClear && <View style={{
                position: 'absolute',
                top: '30%',
                right: 50,
                zIndex : 100
            }}>
                <Button onPress={onClear} buttonStyle={{padding : 0}} type='clear'>
                <AntDesign color={colorDisabledText} size={16} name="closecircle"/>
                </Button>
            </View>}
            <ErrorMessage errors={errors} name={name} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        backgroundColor: '#F2F7FF',
        borderRadius: 15,
        paddingHorizontal: 25,
    },
    placeholder: {
        color: colorPrimaryBold,
        fontWeight : '500'
    },
    text: {
        fontSize: 14,
        fontWeight: '600',
        color: colorPrimary,
        maxWidth : '80%'
    },
    dropDownContainer: {
        borderWidth: 0,
        borderRadius: 20,
        backgroundColor: '#F2F7FF',
        maxHeight: 1000,
    },
    listItemContainer: {
        borderWidth: 0,
        borderTopWidth: 1,
        borderTopColor: '#ECECED',
        
    },
    listItemLabel: {
        color: colorPrimary,
        fontWeight : '500',
        lineHeight : 19
    },
    searchText: {
        color: colorPrimary,
        fontWeight : '400',
        borderWidth : 0,
        backgroundColor : colorBorderGray,
        paddingVertical : 10,
    },
    searchContainer: {
        paddingVertical : 20,
        borderBottomWidth : 0,
        justifyContent : 'space-around',
    },
});