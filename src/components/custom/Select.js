import { useState } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';

export default function Select({options = [],placeholder}) {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [option, setOption] = useState(options);
  
  return (
    <DropDownPicker
      listMode='SCROLLVIEW' // use can Replace with MODAL
      open={open}
      value={value}
      items={option}
      setOpen={setOpen}
      setValue={setValue}
      setItems={setOption}
      placeholder={placeholder || "Vui lòng chọn"}
    />
  );
}