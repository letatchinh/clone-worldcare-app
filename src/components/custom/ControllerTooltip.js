import { Tooltip } from '@rneui/themed';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';

const ControllerTooltip = ({containerStyle,...props}) => {
    const [open, setOpen] = useState(false);
    return (
        <Tooltip
            visible={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            containerStyle={{...styles.container,...containerStyle}}
            {...props}
        />
    );
  
}

export default ControllerTooltip;

const styles = StyleSheet.create({
    container : {
        width : 'auto',
        height : 'auto',
    }
})