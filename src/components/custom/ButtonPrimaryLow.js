import { Button } from '@rneui/base'
import React from 'react'
import { backgroundColorPrimary, backgroundColorPrimaryBold, colorPrimary } from '../../styles/variable'

const ButtonPrimaryLow = ({children,width='100%',buttonStyle={},containerStyle={},textStyle={},onPress=() => {},...props}) => {
  return (
    <Button 
    onPress={onPress}
    containerStyle={{
      width,
        ...containerStyle
    }}
    buttonStyle={{
        backgroundColor : backgroundColorPrimaryBold,
        paddingVertical : 18,
        paddingHorizontal : 16,
        borderRadius : 100,
        ...buttonStyle
    }}
    titleStyle={{
        textAlign : 'center',
        color : colorPrimary,
        fontWeight : '700',
        fontSize : 14,
        ...textStyle
    }}
    {...props}
    >
        {children}
    </Button>
  )
}

export default ButtonPrimaryLow;
