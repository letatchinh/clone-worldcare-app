import { View, Text, Platform } from 'react-native'
import React from 'react'
import ControllerTooltip from './ControllerTooltip'
import { disabledColorBackground } from '../../styles/variable'
import { Badge } from '@rneui/themed'

const sizeExport = {
    md : {
        width :  18,
        height : 18
    },
    sm : {
        width :  14,
        height : 14
    }
}
const ToolTipCustom = ({ popover, children, value, top, left, right, bottom,size = 'md',bgBadge, ...props }) => {
    return (
        <ControllerTooltip
            {...props}
            containerStyle={{ width: 200 }}
            popover={popover}
        >
            {children}
            <Badge
                Component={() => <View style={{
                    backgroundColor: bgBadge ?? disabledColorBackground,
                    borderRadius: 100,
                    alignItems : 'center',
                    justifyContent : 'center',
                    ...sizeExport[size]
                }}>
                    <Text style={{ fontSize: 10, color: 'white' }}>{value}</Text>
                </View>}
                containerStyle={{
                    position: 'absolute',
                    ...top && {top},
                    ...right && {right},
                    ...left && {left},
                    ...bottom && {bottom},
                }}
            />
        </ControllerTooltip>
    )
}

export default ToolTipCustom;
