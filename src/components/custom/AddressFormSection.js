import { get } from 'lodash';
import React, { useMemo, useState } from 'react';
import { Controller } from 'react-hook-form';
import { StyleSheet, View } from 'react-native';
import { useCities, useDistricts, useWards } from '../../hook/geo';
import { colorBorderGray } from '../../styles/variable';
import InputCustom from '../FormComponents/InputCustom';
import TextCustom from '../TextCustom/TextCustom';
import SelectCustom from './SelectCustom';

const AddressFormSection = ({
  watch,
  isLoading,
  cityCode,
  setCityCode,
  districtCode,
  setDistrictCode,
  control,
  type = 'primary',
  leftIconCity,
  leftIconDistrict,
  leftIconWard,
  leftIconInput,
  ArrowDownIconComponent,
  ArrowUpIconComponent,
  errors,
  typeSelect = 'primary',
  showLabel = false,
}) => {
  const [cities, isCitiesLoading] = useCities();
  const [_cityCode, _setCityCode] = useState(cityCode);
  const newCityCode = useMemo(() => cityCode, [cityCode, _cityCode]);
  const [districts, isDistrictsLoading] = useDistricts(newCityCode);
  const [wards, isWardsLoading] = useWards(districtCode);
  const optionsCities = useMemo(() => cities?.map(item => ({
    value: get(item, 'code'),
    label: get(item, 'name'),
  })), [cities]);
  const optionsDistricts = useMemo(() => districts?.map(item => ({
    value: get(item, 'code'),
    label: get(item, 'name'),
  })), [districts]);
  const optionsWards = useMemo(() => wards?.map(item => ({
    value: get(item, 'code'),
    label: get(item, 'name'),
  })), [wards]);
  watch(['address.cityId', 'address.districtId']); // Watch Value Change to ReRender


  return (
    <>
      <View>
        {showLabel && <TextCustom.Roboto style={styles.label}>Tỉnh/Thành phố</TextCustom.Roboto>}
        <Controller
          name={'address.cityId'}
          rules={{ required: true }}
          control={control}
          render={({ field: { onChange, value } }) => {
            return (
              <SelectCustom
                type={typeSelect}
                listMode='MODAL'
                value={value}
                setValue={(callback) => onChange(callback())}
                options={optionsCities}
                placeholder="Tỉnh/Thành phố"
                style={{ ...type === 'outline' ? styles.outlineContainer : {} }}
                ArrowDownIconComponent={ArrowDownIconComponent}
                ArrowUpIconComponent={ArrowUpIconComponent}
                leftIcon={leftIconCity}
                stylePlaceholder={{ ...type === 'outline' ? styles.outlinePlaceholder : {} }}
                name='address.cityId'
                errors={errors}
              />
            )
          }}
        />
      </View>
      <View>
        {showLabel && <TextCustom.Roboto style={styles.label}>Quận/Huyện</TextCustom.Roboto>}
        <Controller
          name={'address.districtId'}
          rules={{ required: true }}
          control={control}
          render={({ field: { onChange, value } }) => {
            return (
              <SelectCustom
                type={typeSelect}
                listMode='MODAL'
                value={value}
                setValue={(callback) => onChange(callback())}
                options={optionsDistricts}
                placeholder="Quận/Huyện"
                style={{ ...type === 'outline' ? styles.outlineContainer : {} }}
                ArrowDownIconComponent={ArrowDownIconComponent}
                ArrowUpIconComponent={ArrowUpIconComponent}
                leftIcon={leftIconDistrict}
                stylePlaceholder={{ ...type === 'outline' ? styles.outlinePlaceholder : {} }}
                name='address.districtId'
                errors={errors}
              />
            )
          }}
        />
      </View>
      <View>
        {showLabel && <TextCustom.Roboto style={styles.label}>Phường/Xã</TextCustom.Roboto>}
        <Controller
          name={'address.wardId'}
          rules={{ required: true }}
          control={control}
          render={({ field: { onChange, value } }) => {
            return (
              <SelectCustom
                type={typeSelect}
                listMode='MODAL'
                value={value}
                setValue={(callback) => onChange(callback())}
                options={optionsWards}
                placeholder="Phường/Xã"
                style={{ ...type === 'outline' ? styles.outlineContainer : {} }}
                ArrowDownIconComponent={ArrowDownIconComponent}
                ArrowUpIconComponent={ArrowUpIconComponent}
                leftIcon={leftIconWard}
                stylePlaceholder={{ ...type === 'outline' ? styles.outlinePlaceholder : {} }}
                name='address.wardId'
                errors={errors}
              />
            )
          }}
        />
      </View>
      <View>
        {showLabel && <TextCustom.Roboto style={styles.label}>Tên đường</TextCustom.Roboto>}
        <Controller
          name={'address.street'}
          rules={{ required: true }}
          control={control}
          render={({ field: { onChange, value } }) => {
            return (
              <InputCustom
                type={typeSelect}
                onChangeText={onChange}
                value={value}
                styleContainer={{ ...type === 'outline' ? styles.outlineContainerInput : {} }}
                styleInput={{ ...type === 'outline' ? styles.outlineInput : {} }}
                placeholder="Nhập địa chỉ của bạn"
                leftIcon={leftIconInput}
                name='address.street'
                errors={errors}
              />
            )
          }}
        />
      </View>
    </>
  )
}

export default AddressFormSection;

const styles = StyleSheet.create({
  outlineContainer: {
    borderColor: colorBorderGray,
    borderWidth: 1,
    backgroundColor: 'transparent',
  },
  outlineContainerInput: {
    borderColor: colorBorderGray,
    borderWidth: 1,
    backgroundColor: 'white'
  },
  outlineInput: {
    paddingLeft: 10,
  },
  outlinePlaceholder: {
    color: '#8896A1'
  },
  label: {
    marginBottom: 10,
    fontWeight : '400'
  }
})