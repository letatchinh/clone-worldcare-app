import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import ButtonPrimary from './ButtonPrimary';
import {  colorPrimaryBold } from '../../styles/variable';
import TextPrimary from '../TextCustom/TextPrimary';
import { Button } from '@rneui/themed';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { GENDER } from '../../constants/defaultValue';
import { Shadow } from 'react-native-shadow-2';

const Item = ({ active,onPress, label,icon }) => {
    return !active
    ? <Button containerStyle={{width : '50%'}} onPress={onPress} type='clear'>
    <View style={styles.box}>
        {icon}<TextPrimary fw={'700'}>{label}</TextPrimary>
    </View>
    </Button>
    : <ButtonPrimary buttonStyle={styles.btn} width={'50%'} >
        {icon}<Text></Text><TextPrimary color={'white'}>{label}</TextPrimary>
    </ButtonPrimary>
}

const SwitchGender = ({value,onChange}) => {
    return (
        <Shadow distance={2} offset={[0,1]}>
            <View style={styles.container}>
            <Item onPress={() => onChange(GENDER.F.en)} icon={<MaterialCommunityIcons name='gender-male' size={20} color={value === GENDER.F.en ? 'white' : colorPrimaryBold}/>} label={GENDER.F.vi} active={value === GENDER.F.en}/>
            <Item onPress={() => onChange(GENDER.M.en)} icon={<MaterialCommunityIcons name='gender-female' size={20} color={value === GENDER.M.en ? 'white' : colorPrimaryBold}/>} label={GENDER.M.vi} active={value === GENDER.M.en}/>
        </View>
        </Shadow>
    )
}

export default SwitchGender;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FAFAFA',
        borderRadius: 18,
        // elevation : 2,
        flexDirection: 'row',
        alignItems: 'center',
        width : '100%'
    },
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems : 'center',
    },
    btn: {
        borderRadius: 20,
        paddingVertical: 12,
    }
})