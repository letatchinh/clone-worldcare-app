import { View, Text, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import { Button } from '@rneui/themed'
import DatePicker from 'react-native-date-picker'
import moment from 'moment'
import { colorPrimary, colorPrimaryBold } from '../../styles/variable'
import ErrorMessage from '../FormComponents/ErrorMessage'

const typeExport = {
  primary : {
    container : {
      backgroundColor : '#F2F7FF',
    },
    input : {
      color : colorPrimary,
      fontWeight : '700',
    },
  },
  secondary : {
    container : {
      backgroundColor : '#FAFAFA',
      elevation : 2,

    },
    input : {
      color : colorPrimaryBold,
      fontWeight : '700',
    },
  },
}

const DatePickerCustom = ({ value, onChange, rightIcon, labelStyle = {}, format = "YYYY-MM-DD", minimumDate, maximumDate, placeholder = "Chưa chọn ngày", errors, name,mode='date' ,type = 'primary',style}) => {
  const [open, setOpen] = useState(false);
  return (
    <View>
      <Button containerStyle={{overflow : 'visible'}} titleStyle={{textAlign : 'right'}} buttonStyle={{...styles.container,  ...typeExport[type].container,...style}} onPress={() => {
        setOpen(true);
        if (!value) {
          onChange(new Date())
        }
      }}>
        <Text style={{
          ...styles.text,
          ...typeExport[type].input,
          ...labelStyle
        }}>  {value ? moment(value).format(format) : placeholder}</Text>
        {rightIcon && rightIcon}
      </Button>
      {value ? <DatePicker
      locale='vi'
        modal
        mode={mode}
        open={open}
        date={value}
        minimumDate={minimumDate}
        maximumDate={maximumDate}
        onConfirm={(date) => {
          setOpen(false)
          onChange(date)
        }}
        onCancel={() => {
          onChange(null);
          setOpen(false);
        }}
        confirmText="Chọn"
        cancelText='Huỷ'
        title={"Chọn ngày"}
      /> : null}
      <ErrorMessage errors={errors} name={name} />
    </View>
  )
}

export default DatePickerCustom;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F2F7FF',
    paddingLeft: 20,
    paddingRight: 25,
    // paddingVertical: 15,
    borderRadius: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  text: {
    color: colorPrimary,
    fontWeight: '600',
    fontSize: 14,
  }
})