import { Button } from '@rneui/themed';
import React, { useCallback, useRef } from 'react';
import { Animated, Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import fontFamily from '../../assets/fonts/fontFamily';
import Stack from '../../components/common/Stack';
import { colorBorderGray, colorPrimary, colorPrimaryBold } from '../../styles/variable';

const stickyHeader = {
    stickyHeaderHiddenOnScroll: false,// Use 'false' to show the sticky header
    stickyHeaderIndices: [0],  // use First child as sticky header
}

const ParallaxHeader = ({ navigation,children,titleHeader,image,heightParallax = 280,backgroundColorHeader }) => {
    const scrollY = useRef(new Animated.Value(0)).current;
    const marginContentParallax = scrollY.interpolate({
        inputRange: [0, heightParallax + 800],
        outputRange: [heightParallax - 20, 0],
        extrapolate: 'clamp',
    });
    const opacityImageParallax = scrollY.interpolate({
        inputRange: [0, heightParallax - 100],
        outputRange: [1, 0.5],
        extrapolate: 'clamp',
    });
    const scaleImageParallax = scrollY.interpolate({
        inputRange: [0, heightParallax - 100],
        outputRange: [1, 1.6],
        extrapolate: 'clamp',
    });
    const translateYImageParallax = scrollY.interpolate({
        inputRange: [0, heightParallax - 100],
        outputRange: [0, heightParallax - 150],
        extrapolate: 'clamp',
    });

    const opacityTitleHeaderParallax = scrollY.interpolate({
        inputRange: [0, heightParallax - 110, heightParallax - 100],
        outputRange: [0, 0, 1],
        extrapolate: 'clamp',
    });
    const translateYTitleHeaderParallax = scrollY.interpolate({
        inputRange: [0, heightParallax - 110, heightParallax -100],
        outputRange: [-10, -10, 0],
        extrapolate: 'clamp',
    });

    const goBack = useCallback(() => navigation.goBack(), [])
    return (
        <Animated.ScrollView
            {...stickyHeader}
            onScroll={Animated.event(
                [{
                    nativeEvent: {
                        contentOffset: {
                            y: scrollY,
                        }
                    }
                }],
                {useNativeDriver: false}
            )}
            style={styles.container}
        >
            <Animated.View style={styles.headerBar}>
                <Stack justify='space-between' align='center'>
                    <View>
                        <TouchableOpacity style={{ padding: 0 }} onPress={goBack} type='clear'>
                            <AntDesign name='left' size={22} color={colorPrimaryBold} />
                        </TouchableOpacity>
                    </View>
                    <Stack align='center'>
                        <Animated.Text style={{opacity : opacityTitleHeaderParallax,transform : [{translateY : translateYTitleHeaderParallax}],color : colorPrimary,fontFamily : fontFamily.roboto}} color={colorPrimary}>{titleHeader}</Animated.Text>
                    </Stack>
                    <View>
                        <Button type='clear'>
                        </Button>
                    </View>
                </Stack>
            </Animated.View>
            <Animated.View style={{
                ...styles.parallaxHeader,
                height : heightParallax,
                opacity: opacityImageParallax,
                transform : [{scale : scaleImageParallax},{translateY : translateYImageParallax}],
                ...backgroundColorHeader && {backgroundColor : backgroundColorHeader}
            }}>
                <Image style={{ width: '100%', height: '100%' }} resizeMode='contain' source={image} />
            </Animated.View>
            <Animated.View style={{
                ...styles.viewContent,
                marginTop : marginContentParallax
            }}>
                {children}
            </Animated.View>


        </Animated.ScrollView>
    )
}

export default ParallaxHeader;

const styles = StyleSheet.create({
  headerBar: {
    backgroundColor: 'white',
    paddingTop: 40,
    paddingBottom: 10,
    paddingHorizontal: 10,
    borderBottomColor: colorBorderGray,
    borderBottomWidth: 1,
  },
  viewContent: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: 'white',
    zIndex: 2,
    paddingHorizontal: 20,
  },
  container: {
    flex: 1,
    padding: 0,
    backgroundColor: colorPrimary + '10',
  },
  parallaxHeader: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    top: 60,
    zIndex: -1,
  },
});