import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { colorPrimary} from '../../styles/variable'
import Stack from '../common/Stack'
/**
 * 
 * color Must be a hex string like #ffffff
 * @returns {JSX}

 */
const Tag = ({children,color=colorPrimary,noBorder=false,style,icon}) => {
  return (
    <Stack 
        style={{
        ...styles.container,
        borderColor : color + "60",
        backgroundColor : color + "10",
        borderWidth : noBorder ? 0 : 1,
        ...style
        }}>
      {icon && icon}
      <Text style={{color,width : 'auto'}}>{children}</Text>
    </Stack>
  )
}

export default Tag;

const styles = StyleSheet.create({
    container : {
        width : 'auto',
        height : 30,
        paddingHorizontal : 4,
        paddingVertical : 5,
        borderRadius : 4,
        justifyContent : 'center',
        alignContent : 'center',
        alignSelf: 'flex-start'
    },
})