import { Skeleton } from '@rneui/themed'
import React from 'react'
import LinearGradient from 'react-native-linear-gradient'

const Row = ({width = 80,height = 40}) => {
    return (
        <Skeleton
            LinearGradientComponent={LinearGradient}
            animation="wave"
            width={width}
            height={height}
        />
    )
}

export default {Row}