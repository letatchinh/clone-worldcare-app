import { Button } from '@rneui/themed'
import React from 'react'
import { Dimensions, StyleSheet, View } from 'react-native'
import ReactNativeModal from 'react-native-modal'
import CommonStyles from '../../styles/common'
import { colorPrimaryLow } from '../../styles/variable'
const widthScreen = Dimensions.get('screen').width;
const heightScreen = Dimensions.get('screen').height;
const ModalCustom = ({ children, isVisible, onCancel = () => { }, onModalWillShow = () => { }, styleContainer = {}, canClose = true, isFullScreen = false, ...props }) => {
  return (
    isVisible ? <ReactNativeModal onBackdropPress={onCancel} onModalWillShow={onModalWillShow} {...props} style={styles.modal} isVisible={isVisible}>
      <View style={CommonStyles.flexCenter}>
        <View style={{ ...styles.container, ...styleContainer, ...isFullScreen && { height: heightScreen, width: widthScreen } }}>
          {canClose && <Button onPress={onCancel} containerStyle={styles.containerBtnClose} titleStyle={{ color: colorPrimaryLow }} buttonStyle={styles.btnClose} type='clear'>
            X
          </Button>}
          {children}
        </View>
      </View>
    </ReactNativeModal> : null
  )
}

export default ModalCustom;

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    backgroundColor: 'white',
    paddingTop: 40,
    paddingBottom: 32,
    paddingHorizontal: 32,
    width: '100%',
    gap: 10,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',

  },
  modal: {
  },
  btnClose: {
    padding: 0,
  },
  containerBtnClose: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 2,
    padding: 10,
    backgroundColor: '#F7FAFF',
    borderRadius: 50
  }
})