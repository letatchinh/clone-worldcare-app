import React from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import { isElement } from '../../utils/helper';
import Stack from '../common/Stack';
import TextPrimary from '../TextCustom/TextPrimary';
import { View } from 'react-native';
import { colorPrimaryBold } from '../../styles/variable';

const Dot = ({ children,dot = true,size }) => {
  return (
    <Stack >
    {dot ? <Entypo name='dot-single' size={18} color={colorPrimaryBold}/> : <View></View>}
      {isElement(children) ? children : <TextPrimary {...size && {size}}>{children}</TextPrimary>}
    </Stack>
  )
}
const Custom = ({icon , children }) => {
  return (
    <Stack align='center'>
      {icon && isReactComponent(icon) ? icon : <TextPrimary>{icon}</TextPrimary>}
      {isReactComponent(children) ? children : <TextPrimary>{children}</TextPrimary>}
    </Stack>
  )
}
const Number = ({ index, children }) => {
  return (
    <Stack align='center'>
      <TextPrimary>{index}</TextPrimary>
      {children}
    </Stack>
  )
}

export default {
  Dot,
  Number,
  Custom,
} 