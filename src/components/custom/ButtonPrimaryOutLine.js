import React from 'react'
import { Button } from '@rneui/base'
import { colorPrimary } from '../../styles/variable'

const exportSize = {
  md: {
    paddingVertical: 18,
    paddingHorizontal: 16,
  },
  sm: {
    paddingVertical: 8,
    paddingHorizontal: 6,
  }
}

const ButtonPrimaryOutLine = ({ children, fullWidth, width, buttonStyle = {}, containerStyle = {}, textStyle = {}, onPress = () => { }, size = 'md', disabled = false, ...props }) => {
  return (
    <Button
      disabled={disabled}
      onPress={onPress}
      containerStyle={{
        ...fullWidth && { width: '100%' },
        ...width && { width },
        borderRadius: 100,
        ...containerStyle
      }}
      buttonStyle={{
        ...exportSize[size],
        ...!disabled && {
          borderWidth: 1,
          backgroundColor: 'transparent',
          borderColor: colorPrimary,
        },
        
        borderRadius: 100,
        ...buttonStyle
      }}
      titleStyle={{
        textAlign: 'center',
        color: colorPrimary,
        ...textStyle
      }}
      {...props}
    >
      {children}
    </Button>
  )
}

export default ButtonPrimaryOutLine;
