import React from 'react'
import { Button } from '@rneui/base'
import { colorPrimary } from '../../styles/variable'
import { StyleSheet } from 'react-native'

const ButtonPrimary = ({children,fullWidth,width,buttonStyle={},containerStyle={},textStyle={},onPress=() => {},active = true,disabled = false,...props}) => {
  return (
    <Button 
    disabled={disabled}
    onPress={onPress}
    containerStyle={{
        ...fullWidth && {width : '100%'},
        ...width && {width},
        ...containerStyle
    }}
    buttonStyle={{
        paddingVertical : 18,
        paddingHorizontal : 16,
        borderRadius : 100,
        ...active ? styles.btnStyleActive : styles.btnStyleInActive,
        ...buttonStyle,
    }}
    titleStyle={{
        textAlign : 'center',
        ...active ? styles.textActive : styles.textInActive,
        ...textStyle,
    }}
    {...props}
    >
        {children}
    </Button>
  )
}

export default ButtonPrimary;

const styles = StyleSheet.create({
  btnStyleActive : {
    backgroundColor : colorPrimary,
  },
  btnStyleInActive : {
    backgroundColor : 'transparent',
    borderColor : colorPrimary,
    borderWidth : 1,
  },
  textActive : {

  },
  textInActive : {

  }
})
