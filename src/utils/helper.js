import { compact, get } from 'lodash';
import Clipboard from '@react-native-clipboard/clipboard';
import { Toast } from 'react-native-toast-message/lib/src/Toast';
import React from 'react';

export const formatPhone = (phone) => phone?.replace(/^(\+84)/, "0");// format +84 with 0
export const formatPhoneV2 = (phone) => phone?.replace(/^0/, "+84");// format 0 with +84

export const getExistProp = (data) => {
  if(!data) return {}
  const result = Object.keys(data).reduce((nextData, key) => {
    if (data[key]) {
      return {
        ...nextData,
        [key]: data[key]
      };
    }

    return nextData;
  }, {});

  return result;
};

export const getPaging = response => {
  const {page,limit,totalDocs,hasNextPage,nextPage} = response
  return {
    current: page,
    pageSize: limit,
    total: totalDocs,
    hasNextPage,
    nextPage,
  }
};

export const convertQueryString = (queryString) => {
  const queryJson = Object.entries(queryString);
  const stringQuery = queryJson.reduce((total, cur, i) => (
    total.concat((i === 0 ? cur[0] ? '?' : '' : '&'), cur[0], '=', encodeURIComponent(cur[1]))
  ), '');
  return stringQuery;
};

export const thousandify = (number, separator = '.') => {
  if (!number && number !== 0) return 0;
  return number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
}

export const concatAddress = (address) => {
  if(!address) return '';
  const { street, ward, district, city } = address;
  return compact([street, ward, district, city])?.join(',');
}
export function removeAccents(str) {
  return str
    .replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a")
    .replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e")
    .replace(/ì|í|ị|ỉ|ĩ/g, "i")
    .replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o")
    .replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u")
    .replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y")
    .replace(/đ/g, "d")
}

export const StringToSlug = (str) => {
  const result = removeAccents(str)
  return result.replaceAll(/\s+/g, '-')
}

export const filterOptionSlug = (input, option) => StringToSlug(get(option, 'label', '')?.toLowerCase())?.includes(StringToSlug(input?.trim()?.toLowerCase()));

export const copy = (str) => {
  try {
    Clipboard.setString(str);
    Toast.show({
      type: 'success',
      text1: 'Copy thành công',
      position: 'bottom'
    })
  } catch (error) {

  }
}

export function isClassComponent(component) {
  return (
      typeof component === 'function' && 
      !!component.prototype.isReactComponent
  )
}

export function isFunctionComponent(component) {
  return (
      typeof component === 'function' && 
      String(component).includes('return React.createElement')
  )
}

export function isReactComponent(component) {
  return (
      isClassComponent(component) || 
      isFunctionComponent(component)
  )
}

export function isElement(element) {
  return React.isValidElement(element);
}

export function isDOMTypeElement(element) {
  return isElement(element) && typeof element.type === 'string';
}

export function isCompositeTypeElement(element) {
  return isElement(element) && typeof element.type === 'function';
};

/**
 * 
 * @param {string} name 
 * @returns {string} Two Characters of Last - 1 and Last
 */
export 
const getShortName = (name) => {
  if(!!!name) return "";
  const arrName = name?.split(' ');
  if(!arrName.length) return "";
  return (arrName[arrName.length - 2]?.charAt(0)||"")+(arrName[arrName.length - 1]?.charAt(0) || "");
}
